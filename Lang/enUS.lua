--------------------------------------------------------------------------------
-- Copyright (c) 2008 Bloodwalker <metagamegeek@gmail.com>
-- 
-- Permission is hereby granted, free of charge, to any person obtaining a copy
-- of this software and associated documentation files (the "Software"), to deal
-- in the Software without restriction, including without limitation the rights
-- to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
-- copies of the Software, and to permit persons to whom the Software is
-- furnished to do so, subject to the following conditions:

-- The above copyright notice and this permission notice shall be included in
-- all copies or substantial portions of the Software.

-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
-- IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
-- FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
-- AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
-- LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
-- OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
-- THE SOFTWARE.
--
--------------------------------------------------------------------------------
-- File:      Lang/enUS.lua
-- Date:      2012-08-17T12:40:42Z
-- Author:    Philosound
-- Version:   v0.96
-- Revision:  123
-- File Rev:  123
-- Copyright: 2008
--------------------------------------------------------------------------------

--[[
    [SystemData.Settings.Language.ENGLISH] = "enUS",
    [SystemData.Settings.Language.FRENCH] = "frFR",
    [SystemData.Settings.Language.GERMAN] = "deDE",
    [SystemData.Settings.Language.ITALIAN] = "itIT",
    [SystemData.Settings.Language.S_CHINESE] = "zhCN",
    [SystemData.Settings.Language.T_CHINESE] = "zhTW",
    [SystemData.Settings.Language.RUSSIAN] = "ruRU",
    [SystemData.Settings.Language.JAPANESE] = "jpJP",
    [SystemData.Settings.Language.KOREAN] = "koKR",

--]]

local ERRMSG = LibStub("WAR-AceLocale-3.0"):NewLocale("MapMonster-Errors", "enUS", true)

-- Missing Data
ERRMSG[101] = "** No zone mapping data found for this zone. For more information on contributing map data please visit the MapMonster page on curse.com"
ERRMSG[102] = "Too early for position info. World not finished loading"
ERRMSG[103] = "Zone data not found."
ERRMSG[104] = "Zone has no map"
ERRMSG[105] = "Pin not found"
ERRMSG[106] = "Icon data not found for subType"
-- Missing Arguments
ERRMSG[201] = "No position table supplied"	
ERRMSG[202] = "Zone Id not found in position table"
ERRMSG[203] = "World coordinates not found in position table"
ERRMSG[204] = "Zone coordinates not found in position table"
ERRMSG[205] = "Incomplete mapIcon definition, texture and scale required"
ERRMSG[206] = "Missing pin data. Must supply two sets of pin data for merging"
ERRMSG[207] = "Pin type not found."
ERRMSG[208] = "Pin type and/or sub type not found"
ERRMSG[209] = "Missing pin type label"
ERRMSG[210] = "Missing pin id"
ERRMSG[211] = "Missing position data. Must supply world coordinate tables for two points for distance calculations"
-- Invalid Arguments
ERRMSG[301] = "Invalid pin property name"
ERRMSG[302] = "Invalid value type for pin property"
ERRMSG[303] = "Invalid pinType option name"
ERRMSG[304] = "Invalid value type for pin type option"
ERRMSG[305] = "Invalid value type. Map icon texture slice must be a string"
ERRMSG[306] = "Invalid tint definition. Map icon texture tint must be a table with r, g, and b keys"
ERRMSG[307] = "Invalid pin type name, must be string"
ERRMSG[308] = "Invalid pin type name, can only contain alphanumeric characters"
ERRMSG[309] = "Invalid pin sub type name, must be string"
ERRMSG[310] = "Invalid pin sub type name, can only contain alphanumeric characters"
ERRMSG[311] = "Invalid pin type or sub type option"
ERRMSG[312] = "Invalid validation type must be \"world\" or \"zone\""
ERRMSG[313] = "Invalid position table. World and zone coordinates do not describe the same location"
ERRMSG[314] = "Invalid position. Coordinates out of bounds for zone"
ERRMSG[315] = "Invalid version numbers"
-- Invalid operation
ERRMSG[401] = "Invalid delete operation. Can ONLY delete options from a pin sub type"
ERRMSG[402] = "Invalid delete operation. Cannot delete multiple options at the same time"
ERRMSG[403] = "Invalid delete operation. Pin is locked"
ERRMSG[404] = "Invalid edit operation. Pin is locked"
ERRMSG[405] = "Invalid direction operation. Cannot plot the direction between zones"
ERRMSG[406] = "Invalid editor mode - Should never see this error. Please report it."
-- Failed operation
ERRMSG[501] = "PinType already registered"
ERRMSG[502] = "PinSubType already registered"
-- General
ERRMSG[601] = "MapMonsterAPI Unavailable"
ERRMSG[602] = "MapMonster minor version out of date"
ERRMSG[603] = "MapMonster major version out of date"

local T = LibStub("WAR-AceLocale-3.0"):NewLocale("MapMonster-Editor", "enUS", true)

-- Editor Labels
T["Map Monster Pin Editor"] = L"Map Monster Pin Editor"
T["New Map Monster Pin"] = L"New Map Monster Pin"
T["Map Monster Pin Viewer"] = L"Map Monster Pin Viewer"
T["Name:"] = L"Name:"
T["Pin Type:"] = L"Pin Type:"
T["Sub Type:"] = L"Sub Type:"
T["Zone:"] = L"Zone:"
T["Position:"] = L"Position:"
T["X:"] = L"X:"
T["Y:"] = L"Y:"
T["Options:"] = L"Options:"
T["Notes:"] = L"Notes:"
T["Datestamp:"] = L"Datestamp:"
T["World:"] = L"World:"
T["Locked"] = L"Locked"
T["Private"] = L"Private"
T["Save"] = L"Save"
T["Reset"] = L"Reset"
T["Cancel"] = L"Cancel"
T["Close"] = L"Close"
-- Messages
T["SuccessAlert"] = L"MapMonster Pin Successfully Saved!"
T["FailedAlert"] = L"MapMonster Pin Update Failed!"
T["NotFoundAlert"] = L"MapMonster Pin Not Found!"

-- PinType Editor Labels

T["Map Monster PinType Editor"] = L"Map Monster PinType Editor"
T["Map Monster PinType Viewer"] = L"Map Monster PinType Viewer"
T["Map Monster SubType Editor"] = L"Map Monster SubType Editor"
T["Map Monster SubType Viewer"] = L"Map Monster SubType Viewer"


T["Merge Radius"] = L"Merge Radius"
T["Map Pin Icon:"] = L"Map Pin Icon:"
T["Change"] = L"Change"
T["Default Sub Type:"] = L"Default Sub Type:"
T["Default Options:"] = L"Default Options:"
T["Red"] = L"Red"
T["Green"] = L"Green"
T["Blue"] = L"Blue"

T["PinTypeSuccessAlert"] = L"MapMonster Pin Type Options Successfully Saved!"

T["PinTypeEditDesc"] = L"Display name used for this pin type in the filter and tooltips"
T["SubTypeComboDesc"] = L"Default sub type used when creating new map pin of this type"
T["SubTypeEditDesc"] = L"Display name used for this sub type in the filter and tooltips"
T["MapIconDesc"] = L"Icon and color used for this pin type to mark pins on the map"
T["RadiusEditDesc"] = L"When merging two pins with matching labels, pin type, and sub type, they must also be within this many inches of each other to be merged.\n Note: If you set this to 0 merging will effectively be disabled for this pin type, if you set this to 65536 matching pins will always be merged"
T["LockedDesc"] = L"New pins created of this type will automatically be marked as locked. Locked pins cannot be edited, merged or changed in any way"
T["PrivateDesc"] = L"New pins created of this type will automatically be marked as private. Private pins cannot be shared with others"

T["Choose Icon..."] = L"Choose Icon..."

T["SubTypeCreatedSuccess"] = L"MapMonster Sub Type Successfully Created!"
T["SubTypeCreatedFailed"] = L"MapMonster Sub Type Creation Failed!"
--[[
T[""] = L""
T[""] = L""
T[""] = L""
T[""] = L""
T[""] = L""
T[""] = L""
T[""] = L""
T[""] = L""
T[""] = L""
T[""] = L""
T[""] = L""
T[""] = L""
T[""] = L""
--]]
