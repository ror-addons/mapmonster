<?xml version="1.0" encoding="UTF-8"?>
<ModuleFile xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<UiMod name="MapMonster" version="0.96" date="06/06/2012">
		<Author name="Wothor; former author Bloodscar" email="" />
		<Description text="Full featured Addon to manage all your MapPins" />
		<VersionSettings gameVersion="1.4.6" windowsVersion="1.0" savedVariablesVersion="1.0" />
		<Dependencies>
			<Dependency name="EA_ChatWindow"/>
			<Dependency name="EA_ContextMenu"/>
			<Dependency name="EA_WorldMapWindow"/>
			<Dependency name="EASystem_Tooltips"/>
			<Dependency name="EA_HelpTips"/>
			<Dependency name="LibSlash"/>
			<Dependency name="LibDateTime-Modified"/>
			<!-- extra dependencies to make sure 
			all the textures are loaded first -->
			<Dependency name="EA_CraftingSystem"/>
			<Dependency name="EA_ScenarioSummaryWindow"/>
			<Dependency name="EATemplate_Icons"/>
			<Dependency name="EA_MenuBarWindow"/>
			<Dependency name="EATemplate_ParchmentWindowSkin"/>
		</Dependencies>
		<Files>
			<File name="Textures/Textures.xml" />
			<File name="Libs/LibStub.lua" />
			<File name="Libs/LibToolkit.lua" />
			<File name="Libs/AceLocale-3.0/AceLocale-3.0.lua" />
			<File name="Lang/enUS.lua" />
			<File name="Lang/deDE.lua" />
			<File name="Source/MapMonster.lua" />
			<File name="Source/MapMonster_Templates.xml" />
			<File name="Source/MapMonster_Tooltips.xml" />
			<File name="Source/MapMonster_Tooltips.lua" />
			<!--@alpha@-->
			<!--<File name="Source/MapMonster_Tools.lua" />-->
			<!--@end-alpha@-->
			<File name="Source/MapMonster_ZoneData.lua" />
			<File name="Source/MapMonster_Zone.lua" />
			<File name="Source/MapMonster_Player.lua" />
			<File name="Source/MapMonster_Navigation.lua" />
			<File name="Source/MapMonster_Pins.lua" />
			<File name="Source/MapMonster_EditorWindow.lua" />
			<File name="Source/MapMonster_EditorWindow.xml" />
			<File name="Source/MapMonster_PinTypeEditorWindow.lua" />
			<File name="Source/MapMonster_PinTypeEditorWindow.xml" />
			<File name="Source/MapMonster_Calibrate.lua" />
			<File name="Source/MapMonster_Calibrate.xml" />
		</Files>

		<OnInitialize>
			<CallFunction name="MapMonster.Initialize" />
			<CallFunction name="MapMonster_Calibrate.Initialize" />
		</OnInitialize>
		<OnShutdown>
			<CallFunction name="MapMonster.ShutdownPlayer" />      
        </OnShutdown> 
		
		
		<OnUpdate>
			<CallFunction name="MapMonster_Tooltip_Update" />
		</OnUpdate>
		
		<SavedVariables>
			<!-- deprecated but needed for conversion -->
			<SavedVariable name="MM_PinTypes"/>
			<SavedVariable name="MM_PinStorage"/>
			<SavedVariable name="MM_Settings"/>	<!-- still used settings are profile style -->
			<!-- new ones that were actually there before the now deprecated ones -->
			<SavedVariable name="MapMonster.PinTypes" global="true" />
			<SavedVariable name="MapMonster.PinStorage" global="true" />
			<SavedVariable name="MapMonster.Settings" global="true" />	<!-- still unused -->
			<SavedVariable name="MapMonster_CalibratedOffset" global="true" />
			<SavedVariable name="MapMonster_CalibratedMapSize" global="true" />
			<!--@alpha@-->
			<!--<SavedVariable name="MapMonster_Tools.Positions" global="true" />-->
			<!--@end-alpha@-->
		</SavedVariables>
	</UiMod>
</ModuleFile>