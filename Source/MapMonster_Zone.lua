--------------------------------------------------------------------------------
-- Copyright (c) 2008 Bloodwalker <metagamegeek@gmail.com>
-- 
-- Permission is hereby granted, free of charge, to any person obtaining a copy
-- of this software and associated documentation files (the "Software"), to deal
-- in the Software without restriction, including without limitation the rights
-- to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
-- copies of the Software, and to permit persons to whom the Software is
-- furnished to do so, subject to the following conditions:

-- The above copyright notice and this permission notice shall be included in
-- all copies or substantial portions of the Software.

-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
-- IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
-- FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
-- AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
-- LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
-- OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
-- THE SOFTWARE.
--
--------------------------------------------------------------------------------
--
-- Starting points for zone offsets and mapsize taken from Nevir27's work on
-- LibSurveyor, all other data and code is original
--
--------------------------------------------------------------------------------
-- File:      Source/MapMonster_Zone.lua
-- Date:      2012-08-17T12:40:42Z
-- Author:    Philosound
-- Version:   v0.96
-- Revision:  123
-- File Rev:  123
-- Copyright: 2008
--------------------------------------------------------------------------------
if not MapMonster or not MapMonster.CoreLoaded then return end --shortcut loading the file if the core didnt load
local LibToolkit = LibStub("LibToolkit-0.1")
--------------------------------------------------------------------------------
--#
--#			Local Definitions
--#
--------------------------------------------------------------------------------
local ZoneInfo = {}

--------------------------------------------------------------------------------
--#
--#			Global Functions for internal use
--#
--------------------------------------------------------------------------------
MapMonster.ZoneFileLoaded = true

function MapMonster.InitializeZones()
	-- protect the zone data
	ZoneInfo = MapMonster.ZoneInfo
	MapMonster.ZoneInfo = nil
end

--------------------------------------------------------------------------------
--#
--#			MapMonster API Functions
--#
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
--#	Function: MapMonsterAPI.GetZoneData(zoneId)
--#		Returns a table with info about the zone.
--#
--#	Parameters:
--#		zoneId -(number) Zone Id to look up
--#	Returns:
--#		zoneInfo -(table) Table containing available zone data
--#	Notes:
--#		Valid keys for zoneInfo are:
--#		offsetX, offsetY, mapsizeX, mapsizeY - for zone with a map 
--#		connect - for zone who connect directly to another without a portal
--#		chunkX, chunkY - for instance and scenario zones *dont need to worry about this*
--#		noMap - for zone where yourplayer icon doesnt show on the map
--#		badZone - for zone with no map at all
--------------------------------------------------------------------------------
function MapMonsterAPI.GetZoneData(zoneId)

	zoneId = tonumber(zoneId)
	local zoneInfo = ZoneInfo[zoneId]
	if not zoneInfo or zoneInfo.badZone then
		MapMonster.ERROR_CODE = 103
		return false
	end

	return LibToolkit.CopyObject(zoneInfo)

end

--------------------------------------------------------------------------------
--#	Function: MapMonsterAPI.WorldToZone(worldPos)
--#		Validates and converts world coordinates into zone coordinates. Return 
--#		new table with full set of coordinates
--#
--#	Parameters:
--#		worldPos -(table) World coordinates as table
--#	Returns:
--#		cleanPos -(table) Return position as table or false on error
--------------------------------------------------------------------------------
function MapMonsterAPI.WorldToZone(worldPos)

	--[===[@alpha@
	--d("WorldToZone")
	--@end-alpha@]===]
	local cleanPos = { zoneId = worldPos.zoneId,
					   worldX = worldPos.worldX,
					   worldY = worldPos.worldY,}
	
	cleanPos = MapMonsterAPI.ValidateWorldPos(cleanPos)
	if not cleanPos then return false end

	local zoneInfo = MapMonsterAPI.GetZoneData(cleanPos.zoneId)
	if not (zoneInfo and zoneInfo.offsetX and zoneInfo.offsetY) then return false end

	-- Simple conversion and rounding zonePos to nearest unit place integer (e.g. 38876)
	cleanPos.zoneX = LibToolkit.roundNum( cleanPos.worldX - zoneInfo.offsetX )
	cleanPos.zoneY = LibToolkit.roundNum( cleanPos.worldY - zoneInfo.offsetY )

	cleanPos = MapMonsterAPI.ValidateZonePos(cleanPos)
	if not cleanPos then return false end
	return cleanPos
end

--------------------------------------------------------------------------------
--#	Function: MapMonsterAPI.ZoneToWorld(zonePos)
--#		Validates and converts zone coordinates into world coordinates. Return 
--#		new table with full set of coordinates
--#
--#	Parameters:
--#		zonePos -(table) Zone coordinates as table
--#	Returns:
--#		cleanPos -(table) Return position as table or false on error
--------------------------------------------------------------------------------
function MapMonsterAPI.ZoneToWorld(zonePos)

	--[===[@alpha@
	--d("ZoneToWorld")
	--@end-alpha@]===]
	local cleanPos = { zoneId = zonePos.zoneId,
					   zoneX = zonePos.zoneX,
					   zoneY = zonePos.zoneY,}

	cleanPos = MapMonsterAPI.ValidateZonePos(cleanPos)
	if not cleanPos then
		return false
	end

	local zoneInfo = MapMonsterAPI.GetZoneData(cleanPos.zoneId)
	if not zoneInfo then
		return false
	end

	cleanPos.worldX = LibToolkit.roundNum(cleanPos.zoneX) + zoneInfo.offsetX
	cleanPos.worldY = LibToolkit.roundNum(cleanPos.zoneY) + zoneInfo.offsetY

	cleanPos = MapMonsterAPI.ValidateWorldPos(cleanPos)
	if not cleanPos then
		return false
	end
	
	return cleanPos

end

--------------------------------------------------------------------------------
--#	Function: MapMonsterAPI.ZoneToShortZone(zonePos)
--#		Validates and converts zone coordinates into short zone coordinates returns
--#		new table with full set of coordinates
--#
--#	Parameters:
--#		zonePos -(table) Zone coordinates as table
--#	Returns:
--#		cleanPos -(table) Return position as table or false on error
--------------------------------------------------------------------------------
function MapMonsterAPI.ZoneToShortZone(zonePos)

	--[===[@alpha@
	--d("ZoneToShortZone")
	--@end-alpha@]===]
	local cleanPos = { zoneId = zonePos.zoneId,
					   worldX = zonePos.worldX,
					   worldY = zonePos.worldY,
					   zoneX  = zonePos.zoneX,
					   zoneY  = zonePos.zoneY,}

	cleanPos = MapMonsterAPI.ValidateZonePos(cleanPos)
	if not cleanPos then return false end

	-- Round zonePos to one decimal place for short zone position (e.g. 38.9)
	cleanPos.zoneX = LibToolkit.roundNum( cleanPos.zoneX / 1000, 1 )
	cleanPos.zoneY = LibToolkit.roundNum( cleanPos.zoneY / 1000, 1 )

	return cleanPos
end

--------------------------------------------------------------------------------
--#	Function: MapMonsterAPI.ValidateWorldPos(dirtyPos)
--#		Convenience function to validate world positions
--#
--#	Parameters:
--#		dirtyPos -(table) World position to validate
--#	Returns:
--#		cleanPos -(table) Return clean postion as table or false on error
--------------------------------------------------------------------------------
function MapMonsterAPI.ValidateWorldPos(dirtyPos)
	--[===[@alpha@
	--d("Validate World Position")
	--@end-alpha@]===]
	return MapMonsterAPI.ValidatePosition(dirtyPos, "world")
end

--------------------------------------------------------------------------------
--#	Function: MapMonsterAPI.ValidateZonePos(dirtyPos)
--#		Convenience function to validate zone positions
--#
--#	Parameters:
--#		dirtyPos -(table) Zone position to validate
--#	Returns:
--#		cleanPos -(table) Return clean postion as table or false on error
--------------------------------------------------------------------------------
function MapMonsterAPI.ValidateZonePos(dirtyPos)
	--[===[@alpha@
	--d("Validate Zone Position")
	--@end-alpha@]===]
	return MapMonsterAPI.ValidatePosition(dirtyPos, "zone")
end

--------------------------------------------------------------------------------
--#	Function: MapMonsterAPI.ValidatePosition(dirtyPos)
--#		Convenience function to validate world and zone positions
--#
--#	Parameters:
--#		dirtyPos -(table) Position table to validate
--#		posType  -(string) Type of validation to run
--#	Returns:
--#		cleanPos -(table) Return clean postion as table or false on error
--#	Notes:
--#		Valid posTypes are "world" and "zone"
--------------------------------------------------------------------------------
function MapMonsterAPI.ValidatePosition(dirtyPos, posType)

	--[===[@alpha@
	--d("Validate Position")
	--@end-alpha@]===]
	
	-- check for position as table
	if type(dirtyPos) ~= "table" then
		MapMonster.ERROR_CODE = 201
		return false
	end
	
	local cleanPos = {}
	cleanPos.zoneId = tonumber(dirtyPos.zoneId)
	cleanPos.worldX = tonumber(dirtyPos.worldX)
	cleanPos.worldY = tonumber(dirtyPos.worldY)
	cleanPos.zoneX  = tonumber(dirtyPos.zoneX)
	cleanPos.zoneY  = tonumber(dirtyPos.zoneY)
	
	-- check for zoneId, cant do anything without it
	if cleanPos.zoneId == nil or cleanPos.zoneId < 1 then
		MapMonster.ERROR_CODE = 202
		return false
	end

	local zoneInfo = ZoneInfo[cleanPos.zoneId]
	-- check have stored zoneInfo to work with
	if zoneInfo == nil or zoneInfo.badZone then
		-- zone that doesnt exist yet
		MapMonster.ERROR_CODE = 103
		return false
	end
	
	local posType = tostring(posType)
	if posType == "nil" then
		posType = "world"
	elseif posType ~= "world" and posType ~= "zone" then
		-- bad validation type
		MapMonster.ERROR_CODE = 312
		return false
	end
	
	if posType == "world" or ( cleanPos.worldX ~= nil and cleanPos.worldX ~= nil ) then
		--[===[@alpha@
		--d("Checking World")
		--@end-alpha@]===]
		-- check world values
		if ( cleanPos.worldX == nil or cleanPos.worldY == nil or
			cleanPos.worldX < 0    or cleanPos.worldY < 0 )
		then
			-- world info completely missing
			MapMonster.ERROR_CODE = 203
			return false
		end

		-- adjust values for instance and scenario zones
		if zoneInfo.chunkX then
			local function _trimWorldPos(oldPos, maxPos)
				local newPos = oldPos
				while newPos > maxPos do
					newPos = newPos - maxPos
				end
				return newPos
			end
			cleanPos.worldX = _trimWorldPos(cleanPos.worldX, zoneInfo.chunkX)
			cleanPos.worldY = _trimWorldPos(cleanPos.worldY, zoneInfo.chunkY)
		end

	end

	-- if the zone has no map we have no way to check the position
	if zoneInfo.noMap then
		if posType == "zone" then
			-- cant have zone pos for a zone with no map
			MapMonster.ERROR_CODE = 104
			return false
		end
		-- return validated position early
		cleanPos.zoneX = nil
		cleanPos.zoneY = nil
		return cleanPos
	end
	
	-- check if we have zoneInfo to verify
	if ( cleanPos.zoneX ~= nil and cleanPos.zoneY ~= nil ) then

		--[===[@alpha@
		--d("Checking Zone")
		--@end-alpha@]===]

		-- if we have a short zone pos expand it
		local shortX = tonumber(string.match(cleanPos.zoneX, "%d+/.%d+"))
		local shortY = tonumber(string.match(cleanPos.zoneY, "%d+/.%d+"))
		if shortX or shortY then
			cleanPos.zoneX = LibToolkit.roundNum(shortX * 1000)
			cleanPos.zoneY = LibToolkit.roundNum(shortY * 1000)
		end
		
		-- no zoneInfo in future maybe; next two ifs would be commented out then
		
		-- if we have world coords check (world - offset = zone), 
		-- otherwise they arent describing the same point
		if ( cleanPos.worldX ~= nil and cleanPos.worldY ~= nil and 
			 cleanPos.worldX >= 0   and cleanPos.worldY >= 0 and 
			( cleanPos.zoneX ~= (cleanPos.worldX - zoneInfo.offsetX) or 
			  cleanPos.zoneY ~= (cleanPos.worldY - zoneInfo.offsetY) ) )
		then
			-- mismatched coordinates
			MapMonster.ERROR_CODE = 313
			return false
		end

		-- check the zone pos is within the map boundaries
		if ( cleanPos.zoneX < 0 or cleanPos.zoneX > zoneInfo.mapsizeX or
			 cleanPos.zoneY < 0 or cleanPos.zoneY > zoneInfo.mapsizeY )
		then
			-- coord out of bound
			MapMonster.ERROR_CODE = 314
			return false
		end

	-- if were checking zone pos then fail it
	elseif posType == "zone" then
		-- no zone info to check
		MapMonster.ERROR_CODE = 204
		return false
	end
		
	-- all went well return a clean set of positions
	return cleanPos
end
