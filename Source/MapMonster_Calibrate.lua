--------------------------------------------------------------------------------
-- Copyright (c) 2008 Bloodwalker <metagamegeek@gmail.com>
-- 
-- Permission is hereby granted, free of charge, to any person obtaining a copy
-- of this software and associated documentation files (the "Software"), to deal
-- in the Software without restriction, including without limitation the rights
-- to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
-- copies of the Software, and to permit persons to whom the Software is
-- furnished to do so, subject to the following conditions:

-- The above copyright notice and this permission notice shall be included in
-- all copies or substantial portions of the Software.

-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
-- IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
-- FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
-- AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
-- LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
-- OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
-- THE SOFTWARE.
--
--------------------------------------------------------------------------------
-- File:      Source/MapMonster_Calibrate.lua
-- Date:      2012-08-17T12:40:42Z
-- Author:    Philosound
-- Version:   v0.96
-- Revision:  123
-- File Rev:  123
-- Copyright: 2008
--------------------------------------------------------------------------------

-- Skip loading completely if theres a problem with MapMonster
if not MapMonster or not MapMonster.CoreLoaded then return end --shortcut loading the file if the core didnt load
if not MapMonsterAPI.CheckVersion(0.3, 2) then return end

local OffSetDatasets = {}
local MapMaxX = 0
local MapMaxY = 0

local startMapOffset = false
local startMapSize = false
local mouseOverEndCount = 0

local shownMap = 0

local calibrateWindowName = "MapMonster_CalibrateWindow"

MapMonster_Calibrate = {}

--
-- Locally defined functions
--
local CHAT_LOG_FILTER = 988
local function print(txt) TextLogAddEntry("Chat", CHAT_LOG_FILTER, L"[MapMonster]: " .. towstring(txt)) end

local function Calibrate_DestroyMap()
	--[===[@alpha@
	d("MapMonster_Calibrate.DestroyMap()")
	--@end-alpha@]===]
	RemoveMapInstance( calibrateWindowName.."MapDisplay" )
end

local function Calibrate_CreateMap(zoneId)

	--[===[@alpha@
	d("Calibrate_CreateMap()")
	--@end-alpha@]===]

	local zoneId = zoneId or GameData.Player.zone
	shownMap = zoneId
	
	Calibrate_DestroyMap()
	
	CreateMapInstance( calibrateWindowName .. "MapDisplay", SystemData.MapTypes.NORMAL )
	MapSetMapView( calibrateWindowName .. "MapDisplay", SystemData.MapLevel.ZONE, zoneId)
	WindowSetTintColor( calibrateWindowName .. "ClickLayer", 255, 0, 0 )
	WindowSetAlpha( calibrateWindowName .. "ClickLayer", 0.2)
	
	-- Reset everything on map creation
	OffSetDatasets = {}
	MapMaxX = 0
	MapMaxY = 0
	mouseOverEndCount = 0

	print(L"Showing : " .. GetZoneName(shownMap) .. L" ( " ..shownMap .. L" ) ")
end

local function Calibrate_OpenWindow()
    if( not DoesWindowExist( calibrateWindowName ) ) then
        CreateWindow( calibrateWindowName, false )
    end
	Calibrate_CreateMap()
	WindowSetShowing( calibrateWindowName .. "MainBackground", false )
end

local function Calibrate_CloseWindow()
	startMapOffset = false
	startMapSize = false
	Calibrate_DestroyMap()
	DestroyWindow( calibrateWindowName )
end

local function Calibrate_MoveAnchor(point)
	WindowClearAnchors( calibrateWindowName .. "MapDisplay" )
	WindowAddAnchor( calibrateWindowName .. "MapDisplay", point, calibrateWindowName .. "Background", point, 0, 0)
end

function Calibrate_ResizeMap(scale) WindowSetScale( calibrateWindowName .. "MapDisplay", scale ) end

--
-- Global calibration functions
--

function MapMonster_Calibrate.Initialize()
	-- Initialize Saved Variables we'll need
	if (MapMonster_CalibratedOffset == nil) then
		MapMonster_CalibratedOffset = {}
	end
	if (MapMonster_CalibratedMapSize == nil) then
		MapMonster_CalibratedMapSize = {}
	end
	LibSlash.RegisterSlashCmd( "calibrate", MapMonster_Calibrate.SlashHandler )
	LibSlash.RegisterSlashCmd( "mapcal", MapMonster_Calibrate.SlashHandler )
end

--
-- Window Creation Functions
--

function MapMonster_Calibrate.WindowInit() LabelSetText( calibrateWindowName .. "Text", L"Zone Calibration Window" ) end

--
-- Window Handling Functions
--

function MapMonster_Calibrate.Show()
    if( not DoesWindowExist( calibrateWindowName ) ) then Calibrate_OpenWindow() end
    WindowSetShowing( calibrateWindowName, true )
end

function MapMonster_Calibrate.Hide() WindowSetShowing( calibrateWindowName, false ) end

function MapMonster_Calibrate.Toggle()
    if ( not DoesWindowExist( calibrateWindowName ) or not WindowGetShowing( calibrateWindowName ) ) then
    	MapMonster_Calibrate.Show()
    else
		MapMonster_Calibrate.Hide()
    end
end

--
-- Window Event handlers
--

function MapMonster_Calibrate.OnHidden()
    WindowUtils.OnHidden()
    Calibrate_CloseWindow()
end

function MapMonster_Calibrate.OnShown() WindowUtils.OnShown( MapMonster_Calibrate.Hide, WindowUtils.Cascade.MODE_AUTOMATIC ) end

function MapMonster_Calibrate.OnLMouseButton()
			
	--local playerPosition = MapMonsterAPI.GetPlayerPosition()
	local playerPosition = MapMonster.GetTruePosition()
	local zoneId = playerPosition.zoneId

	if startMapOffset then
		if ( playerPosition.worldX == nil or playerPosition.worldY == nil or
			 playerPosition.worldX == 0   or playerPosition.worldY == 0 )
		then
			LabelSetText( calibrateWindowName .. "Text", L"You need to move first before you can calibrate the map coordinates" )
			print("You need to move first before you can calibrate the map coordinates")
			return
		end
		if shownMap ~= zoneId then
			LabelSetText( calibrateWindowName .. "Text", L"You can't calculate zone offsets for a zone you aren't in" )
			print("You can't calculate zone offsets for a zone you aren't in")
			return
		end
	else
		return
	end

	-- Get the human readbale map coords of the mouse Click
	local mapPositionX, mapPositionY = WindowGetScreenPosition( calibrateWindowName .. "MapDisplay" )
	local resolutionScale = InterfaceCore.GetResolutionScale()
	local mapX, mapY = MapGetCoordinatesForPoint( calibrateWindowName .. "MapDisplay",
													(SystemData.MousePosition.x - mapPositionX) / resolutionScale,
													(SystemData.MousePosition.y - mapPositionY) / resolutionScale )
    if (mapX == nil or mapY == nil) then
    	LabelSetText( calibrateWindowName .. "Text", L"You need to click on your player icon on the map to calibrate" )
		print("You need to click on your player cursor on the map to calibrate")
		return
    end

	-- Assemble full location data
	local mapOffset = {}

	mapOffset.worldX = playerPosition.worldX
	mapOffset.worldY = playerPosition.worldY
	mapOffset.zoneX = mapX
	mapOffset.zoneY = mapY

	-- Add location data to table to generate offsets
	if not OffSetDatasets[zoneId] then
		OffSetDatasets[zoneId] = {}
	end

	table.insert( OffSetDatasets[zoneId], mapOffset )

	-- if we have 3 captured data sets we can make the offsets
	if ( #OffSetDatasets[zoneId] == 3 ) then

		startMapOffset = false

		local offsetX = 0
		local offsetY = 0

		-- Average the offset from all 3 data sets for improved accuracy
		for i, mapData in pairs( OffSetDatasets[zoneId] ) do
			local x = mapData.worldX - mapData.zoneX
			local y = mapData.worldY - mapData.zoneY
			offsetX = offsetX + x
			offsetY = offsetY + y
		end

		offsetX = offsetX / 3
		offsetY = offsetY / 3

		local zoneName = tostring( GetZoneName(zoneId) )

		-- Print results and change tint to green
		print( "Calculated Offset for " .. zoneName .. "=> X: " .. offsetX .. " Y: " .. offsetY )

		-- Save new offset data 
		table.insert(MapMonster_CalibratedOffset, {offsetX = offsetX, offsetY = offsetY, zoneId = zoneId, zoneName = GetZoneName(zoneId), rawData = OffSetDatasets[zoneId] })

		-- Nuke the save OffSetDatasets
		OffSetDatasets[zoneId] = nil
		
		WindowSetTintColor( calibrateWindowName .. "ClickLayer", 0, 255, 0 )
		Sound.Play(Sound.RVR_FLAG_OFF)
		LabelSetText( calibrateWindowName .. "Text", L"Zone Calibration Complete!" )
		MapMonster_Calibrate.Hide()
	else
		-- play sound to acknowldege mouse Click
		Sound.Play(Sound.MONEY_LOOT)
		WindowSetTintColor( calibrateWindowName .. "ClickLayer", 255, 0, 0 )
		LabelSetText( calibrateWindowName .. "Text", L"Offset Calibration - Click center of green player icon again!" )
	end

end

function MapMonster_Calibrate.OnMouseOverEnd(flags, mouseX, mouseY)

	local playerPosition = MapMonsterAPI.GetPlayerPosition()
	--local zoneId = playerPosition.zoneId
	local zoneId = shownMap
	local zoneName = GetZoneName(zoneId) 
				
		-- Get the human readable map coords of the mouse Click
	local mapPositionX, mapPositionY = WindowGetScreenPosition( calibrateWindowName .. "MapDisplay" )
	local resolutionScale = InterfaceCore.GetResolutionScale()
	local mapX, mapY = MapGetCoordinatesForPoint( calibrateWindowName .. "MapDisplay",
													(SystemData.MousePosition.x - mapPositionX) / resolutionScale,
													(SystemData.MousePosition.y - mapPositionY) / resolutionScale )
	if startMapSize then
--[[
			if ( playerPosition.worldX == nil or playerPosition.worldY == nil or
					 playerPosition.worldX == 0   or playerPosition.worldY == 0 ) 
			then
					LabelSetText( calibrateWindowName .. "Text", L"You need to move first before you can calibrate the map size" )
					print("You need to move first before you can calibrate the map size")
					return
			end		
--]]
			mouseOverEndCount = mouseOverEndCount + 1
			LabelSetText( calibrateWindowName .. "Text", zoneName .. L" Calibration - MouseOver BottomRight Corner " .. mouseOverEndCount .. L"/25" )
	else
		return
	end

	if (mapX > MapMaxX) then MapMaxX = mapX end
	if (mapY > MapMaxY) then MapMaxY = mapY end
	
	-- we done at least 25 mouseovers we can stop now
	if ( mouseOverEndCount > 25 ) then
		startMapSize = false
		
		print("Calculated Map Size for " .. tostring(zoneName) .. "=> X: " .. MapMaxX .. " Y: " .. MapMaxY)
		Sound.Play(Sound.RVR_FLAG_OFF)
		LabelSetText( calibrateWindowName .. "Text", L"Zone Map Size Calibration Complete!" )
		--MapMonster_Calibrate.Hide()
		Calibrate_ResizeMap( 0.875 )
		
		-- Save new mapsize and rebuild list
		MapMonster_CalibratedMapSize[zoneId] = {sizeX = MapMaxY, sizeY = MapMaxY, zoneName = GetZoneName(zoneId)}
		--MapMonster.BuildZoneTables()
	end
end


function MapMonster_Calibrate.SlashHandler(args)

	--[===[@alpha@
	d("slashHandler args=" .. args)
	--@end-alpha@]===]
	local cmd = StringSplit(args)
	
	if (cmd[1] ~= "on" and cmd[1] ~= "open" and (cmd[1] ~= "" and not DoesWindowExist( calibrateWindowName ) ) ) then
		MapMonster_Calibrate.Toggle()
	end
	
	if (cmd[1] == "on" or cmd[1] == "open" or cmd[1] == "off" or cmd[1] == "close") then
		MapMonster_Calibrate.Toggle()
	elseif (cmd[1] == "reset") then
		Calibrate_ResizeMap( 0.875 )
		Calibrate_MoveAnchor( "topleft" )
	elseif (cmd[1] == "scale") then
		local scale = tonumber(cmd[2])
		if ( scale == nil or scale <= 0.1) then
				scale = 0.1
		elseif ( scale > 3 ) then
				scale = 3
		end
		Calibrate_ResizeMap( scale )
	elseif (cmd[1] == "normal") then
		Calibrate_ResizeMap( 0.875 )
	elseif (cmd[1] == "double") then
		Calibrate_ResizeMap( 1.75 )
	elseif (cmd[1] == "topleft" or
					cmd[1] == "topright" or
					cmd[1] == "bottomleft" or
					cmd[1] == "bottomright" or
					cmd[1] == "center" or
					cmd[1] == "top" or
					cmd[1] == "bottom" or
					cmd[1] == "left" or
					cmd[1] == "right"
					) then
		Calibrate_MoveAnchor(cmd[1])
	elseif (cmd[1] == "mapsize" or cmd[1] == "size") then
		startMapSize = true
		Calibrate_ResizeMap( 100 )
		Calibrate_MoveAnchor( "bottomright" )
		LabelSetText( calibrateWindowName .. "Text", L"Map Size Calibration - MouseOver BottomRight Corner" )
	elseif (cmd[1] == "mapoffset" or cmd[1] == "offset") then
		startMapOffset = true
		LabelSetText( calibrateWindowName .. "Text", L"Offset Calibration - Click center of green player icon!" )
	elseif (cmd[1] == "zone") then
		Calibrate_CreateMap(tonumber(cmd[2]))
	else -- Generic help
		print("MapMonster Calibration Help:")
		print("  /calibrate [command [option]]")
		print("    on/open - Start or Open the zone calibration window")
		print("    off/close - Close zone calibration window")
		print("    mapsize - Set scale to 3 and anchor bottomright to get mapsize from corner mouseover")
		print("    mapoffset - Enable mouseclicks on map to calibrate zone offset from player icon position")
		print("    reset - Reset calibration window to normal scale and anchor point")
		print("    scale [option] - Set calibration map to [option] scale (min 0.1, max 3)")
		print("    normal - Set calibration map to a scale of 1")
		print("    double - Set calibration map to a scale of 2")
		print("    topleft - Set map anchor point to top left")
		print("    topcenter - Set map anchor point to top center")
		print("    topright - Set map anchor point to top right")
		print("    bottomleft - Set map anchor point to bottom left")
		print("    bottomcenter - Set map anchor point to bottom center")
		print("    bottomright - Set map anchor point to bottom right")
		print("    center - Set map anchor point to center")
	end
end

function MapMonster_Calibrate.NextMap()
	if not DoesWindowExist( calibrateWindowName ) then MapMonster_Calibrate.Toggle() end
	Calibrate_CreateMap( shownMap + 1)
end

-- Unused
function MapMonster_Calibrate.WindowReport(args)
	local winX, winY = WindowGetDimensions(calibrateWindowName .. "MapDisplay")
	local winScale = WindowGetScale(calibrateWindowName .. "MapDisplay")
	print(args .. ": " .. " scale:" .. winScale .. " X:" .. winX .. " Y:" .. winY)
end

-- Unused
function MapMonster_Calibrate.GetDimensions()
	local winX, winY = WindowGetDimensions(calibrateWindowName .. "MapDisplay")
	local winScale = WindowGetScale(calibrateWindowName .. "MapDisplay")
	print("MapDisplay - scale:" .. winScale .. " X:" .. winX .. " Y:" .. winY)

	local winX, winY = WindowGetDimensions(calibrateWindowName .. "ClickLayer")
	local winScale = WindowGetScale(calibrateWindowName .. "ClickLayer")
	print("ClickLayer - scale:" .. winScale .. " X:" .. winX .. " Y:" .. winY)
end