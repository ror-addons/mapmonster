--------------------------------------------------------------------------------
-- Copyright (c) 2008 Bloodwalker <metagamegeek@gmail.com>
-- 
-- Permission is hereby granted, free of charge, to any person obtaining a copy
-- of this software and associated documentation files (the "Software"), to deal
-- in the Software without restriction, including without limitation the rights
-- to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
-- copies of the Software, and to permit persons to whom the Software is
-- furnished to do so, subject to the following conditions:

-- The above copyright notice and this permission notice shall be included in
-- all copies or substantial portions of the Software.

-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
-- IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
-- FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
-- AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
-- LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
-- OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
-- THE SOFTWARE.
--
--------------------------------------------------------------------------------
--
-- Starting points for zone offsets and mapsize taken from Nevir27's work on
-- LibSurveyor, all other data and code is original
--
--------------------------------------------------------------------------------
-- File:      Source/MapMonster.lua
-- Date:      2012-08-17T12:40:42Z
-- Author:    Philosound
-- Version:   v0.96
-- Revision:  123
-- File Rev:  123
-- Copyright: 2008
--------------------------------------------------------------------------------

local MAJOR_VER = 0.9
local MINOR_VER = 2

local CHAT_LOG_FILTER = 988
local CHAT_LOG_COLOR = DefaultColor.ORANGE

local LibDateTime = LibStub("LibDateTime-Modified-0.4")
local LibToolkit = LibStub("LibToolkit-0.1")
local ERRMSG = LibStub("WAR-AceLocale-3.0"):GetLocale("MapMonster-Errors")


--------------------------------------------------------------------------------
--# Initialize a new chat filter so I can color MapMonster text output to
--# the chat window
--------------------------------------------------------------------------------
LibToolkit.NewChatFilter(CHAT_LOG_FILTER, CHAT_LOG_COLOR)

local function ConfirmCreateSubType( subTypeLabel )
	
	if not subTypeLabel or not string.match( subTypeLabel, "[^%w]+") then
		subTypeLabel = "MapMonster Sub Type"
	end
	subTypeLabel = towstring(subTypeLabel)
	
	local confirmLabel = L"Create new map pin type:\n\nCategory: " .. MapMonsterAPI.GetPinTypeOption( MapMonster.GENERAL_PINTYPE, nil, "label" ) .. L"\nSub Type: " .. subTypeLabel

	local function blank() end
	
	local function create()
		local pinType = MapMonster.GENERAL_PINTYPE
		local subType = "UserSubType" .. MapMonster.Settings.LastUserType
		local created = MapMonsterAPI.CreatePinSubType( pinType, subType, { label = subTypeLabel } )
		if created then
			MapMonster.Settings.LastUserType = MapMonster.Settings.LastUserType + 1
			AlertTextWindow.AddLine (MapMonster.AlertTypes.SUCCESS, T["SubTypeCreatedSuccess"])
			MapMonster.PinTypeEditor.OpenTypeEditor( pinType, subType )
		else
			AlertTextWindow.AddLine (MapMonster.AlertTypes.ERROR, T["SubTypeCreatedFailed"])
			local errorCode, errorMessage = MapMonsterAPI.GetError()
			AlertTextWindow.AddLine (MapMonster.AlertTypes.ERROR, errorMessage)
		end
	end	

	DialogManager.MakeTwoButtonDialog( confirmLabel, L"Yes", create, L"No", blank, nil)
	
end

--------------------------------------------------------------------------------
--# End chat filter init
--------------------------------------------------------------------------------

MapMonster = {}
MapMonsterAPI = {}

MapMonster.CoreLoaded = true

MapMonster.ERROR_CODE = 0

MapMonster.AlertTypes = {}
MapMonster.AlertTypes.SUCCESS 	 = SystemData.AlertText.Types.ENTERZONE
MapMonster.AlertTypes.ERROR 	 = SystemData.AlertText.Types.MOVEMENT_RVR
MapMonster.AlertTypes.WP_ARRIVED = SystemData.AlertText.Types.QUEST_END
--AlertTextWindow.AddLine (alertType, text)

--------------------------------------------------------------------------------
--#
--#			Local Functions
--#
--------------------------------------------------------------------------------

local function DisableMapMonsterAPI()

	d("Disabling MapMonsterAPI")
	local metatable = {}
	metatable.__index = function(table, key)
		local busted = function() return false end
		return busted
	end
	
	MapMonsterAPI = {}
	setmetatable(MapMonsterAPI, metatable)

	MapMonsterAPI.GetError = function() return MapMonster.ERROR_CODE, ERRMSG[MapMonster.ERROR_CODE] end
	MapMonsterAPI.CheckVersion = function() return false end

	MapMonster.ERROR_CODE = 601
end

--------------------------------------------------------------------------------
--#
--#			Global Functions for internal use
--#
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
--# Printing function to output colored text to chat window
--#
--------------------------------------------------------------------------------
function MapMonster.print(txt)
	LibToolkit.print(CHAT_LOG_FILTER, "[MapMonster]", txt)
end

--------------------------------------------------------------------------------
--#	Various sorting functions for table.sort
--#
--------------------------------------------------------------------------------
function MapMonster.AlphabetizeByName( table1, table2 )
    return LibToolkit.AlphabetizeByField( table1, table2, "name" )
end

function MapMonster.CustomSortByLabel( table1, table2 )
	return MapMonster.CustomSortByField( table1, table2, "label")
end
function MapMonster.CustomSortByName( table1, table2 )
	return MapMonster.CustomSortByField( table1, table2, "name")
end

function MapMonster.CustomSortByField( table1, table2, field )

    if( table2 == nil ) then return false end
    if( table1 == nil ) then return false end
    if( table1[field] == nil or table2[field] == nil ) then
        ERROR(L"Table must contain specified field for CustomSortByField." )
        return false
    end
        
    -- Sort default entries to top of the list
    if( table1[field] == L"Default" ) then return true end
    if( table2[field] == L"Default" ) then return false end

    -- Sort MapMonster entries to top of the list
    if( table1[field] == L"Map Monster" ) then return true end
    if( table2[field] == L"Map Monster" ) then return false end

    return (table1[field] < table2[field])
end

--------------------------------------------------------------------------------
--#
--#			Global MapMonster Event Handlers and Functions
--#
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
--#	MapMonster.Initialize()
--#		Initial setup of MapMonster, calls all other init functions from other
--# modules.
--#
--------------------------------------------------------------------------------
function MapMonster.Initialize()

	d("Initializing MapMonster v" .. MAJOR_VER .. "." .. MINOR_VER)

	-- did all the files load without error
	if ( not MapMonster.TooltipsFileLoaded or
		 not MapMonster.ZoneFileLoaded or
		 not MapMonster.PlayerFileLoaded or
		 not MapMonster.NavigationFileLoaded or
		 not MapMonster.PinFileLoaded )
	then
		-- disable MapMonsterAPI
		DisableMapMonsterAPI()
		d("File Loading Error MapMonster v" .. MAJOR_VER .. "." .. MINOR_VER)
		MapMonster.print("Error Loading MapMonster v" .. MAJOR_VER .. "." .. MINOR_VER)
		return
	end
	-- did all the inits run properly
	local loadtooltip, errorStrings1 = pcall(MapMonster.InitializeTooltips)
	local loadzone, errorStrings2    = pcall(MapMonster.InitializeZones)
	local loadplayer, errorStrings3  = pcall(MapMonster.InitializePlayer)
	local loadpins, errorStrings4    = pcall(MapMonster.InitializeMapPins)
	local loadeditor, errorStrings5  = pcall(MapMonster.Editor.Initialize)
	local loadtypeeditor, errorStrings6  = pcall(MapMonster.PinTypeEditor.Initialize)
	if ( not loadtooltip or
		 not loadzone or
		 not loadplayer or
		 not loadpins or
		 not loadeditor or
		 not loadtypeeditor )
	then
		-- we got an init error shut everything down that loaded before the error
		if loadtooltip then MapMonster.ShutdownTooltips() end
		if loadplayer then MapMonster.ShutdownPlayer() end
		if loadpins then MapMonster.ShutdownPins() end
		if loadeditor then MapMonster.Editor.Shutdown() end
		if loadtypeeditor then MapMonster.PinTypeEditor.Shutdown() end
		--disable the MapMonsterAPI
		DisableMapMonsterAPI()
		d("Error Initializing MapMonster v" .. MAJOR_VER .. "." .. MINOR_VER)
		d(errorStrings1)
		d(errorStrings2)
		d(errorStrings3)
		d(errorStrings4)
		d(errorStrings5)
		d(errorStrings6)
		MapMonster.print("Error Loading MapMonster v" .. MAJOR_VER .. "." .. MINOR_VER)
		return
	end

	-- Everything loaded fine so setup the hooks
	MapMonster.InitializeHooks()
	MapMonster.Editor.InitializeHooks()

	-- Register slash command for MapMonster
	LibSlash.RegisterSlashCmd("mapmonster", MapMonster.SlashHandler)
	LibSlash.RegisterSlashCmd("mm", MapMonster.SlashHandler)

	d("MapMonster Successfully loaded")
	MapMonster.print("MapMonster Successfully loaded v" .. MAJOR_VER .. "." .. MINOR_VER)

end

--------------------------------------------------------------------------------
--#	MapMonster.SlashHandler(command)
--#		Handles all chat window commands sent using LibSlash
--#
--------------------------------------------------------------------------------
function MapMonster.SlashHandler(command)

	--[===[@alpha@
	d("slashHandler args=" .. command)
	--@end-alpha@]===]
	local cmd = StringSplit(command)
	if (cmd[1] == "version" or cmd[1] == "ver") then
		MapMonster.print("MapMonster v" .. MAJOR_VER .. "." .. MINOR_VER .. " Loaded")
	elseif (cmd[1] == "new") then
		local newPos = MapMonsterAPI.GetPlayerPosition()
		if newPos.worldX == nil then
			MapMonster.print("You need to move first before attempting to create a pin at your location")
			return
		end
		MapMonster.Editor.OpenAddWindow(newPos)
	elseif (cmd[1] == "newtype") then
		local subTypeLabel = LibToolkit.CopyObject( cmd )
		table.remove(subTypeLabel, 1)
		if not LibToolkit.IsTableEmpty(subTypeLabel) then
			ConfirmCreateSubType( table.concat(subTypeLabel, " ") )
		else
			ConfirmCreateSubType()
		end
	elseif (cmd[1] == "highlight") then
		if cmd[2] == "" or cmd[3] == "" or cmd[4] == "" then return end
		local r, g, b = tonumber(cmd[2]), tonumber(cmd[3]), tonumber(cmd[4])
		if r < 0 or r > 255 or g < 0 or g > 255 or b < 0 or b > 255 then return end
		MapMonster.Settings.FlashColor = { r = r, g = g, b = b}
		MapMonster.HighlightSetTint()
		MapMonster.print("Map pin highlight changed to: red = " .. r .. " green = " .. g .. " blue = " .. b)
	elseif (cmd[1] == "filters" or cmd[1] == "filter" ) then
		if (cmd[2] == "reset") then
			MapMonster.ResetFilters()
			MapMonster.print("Reset All MapMonster Filters")
		end
	else -- Generic help
		MapMonster.print("MapMonster Help:")
		MapMonster.print("  /mapmonster [command [option]] or /mm")
		MapMonster.print("    new     - Creates a new MapPin at the players current location")
		MapMonster.print("    version - Print the current version of MapMonster to chat window")
		MapMonster.print("    highlight [red] [green] [blue] - Changes the animated highlight arrows to specified color")
	end

end

--------------------------------------------------------------------------------
--#
--#			MapMonster API Functions
--#
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
--#	Function: MapMonsterAPI.CheckVersion(major, minor)
--#		Verifies if the current version of MapMonster is equal or newer
--#
--#	Parameters:
--#		major - (number) Major version number to check
--#		minor - (number) Minor version number to check
--#	Returns:
--#		boolean - (boolean) Returns true if equal or newer, false otherwise
--#
--------------------------------------------------------------------------------
function MapMonsterAPI.CheckVersion(major, minor)

	major = tonumber(string.match(major, "%d+"))
	minor = tonumber(string.match(minor, "%d+"))

	-- Check supplied major and minor
	if (major == nil or major < 0 or
	    minor == nil or minor < 0) 
	then
		MapMonster.ERROR_CODE = 315
		return false -- bad version number so we fail
	end

	-- Check the addon version
	if ( MAJOR_VER > major ) then
		return true -- major version larger its all good
	elseif ( MAJOR_VER == major ) then
		if ( MINOR_VER >= minor ) then
			return true -- Major and Minor version are good
		else
			MapMonster.ERROR_CODE = 602
			return false -- Addon minor version to old
		end
	else
		MapMonster.ERROR_CODE = 603
		return false -- Addon major version to old
	end

end

--------------------------------------------------------------------------------
--#	Function: MapMonsterAPI.GetError()
--#		Get the last error code and string from MapMonster
--#
--#	Parameters:
--#		none
--#	Returns:
--#		errorcode	-(number) Numeric MapMonster error code
--#		errorstring	-(string) Localized error message for error code
--------------------------------------------------------------------------------
function MapMonsterAPI.GetError() return MapMonster.ERROR_CODE, ERRMSG[MapMonster.ERROR_CODE] end
