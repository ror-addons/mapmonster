--------------------------------------------------------------------------------
-- Copyright (c) 2008 Bloodwalker <metagamegeek@gmail.com>
-- 
-- Permission is hereby granted, free of charge, to any person obtaining a copy
-- of this software and associated documentation files (the "Software"), to deal
-- in the Software without restriction, including without limitation the rights
-- to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
-- copies of the Software, and to permit persons to whom the Software is
-- furnished to do so, subject to the following conditions:

-- The above copyright notice and this permission notice shall be included in
-- all copies or substantial portions of the Software.

-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
-- IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
-- FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
-- AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
-- LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
-- OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
-- THE SOFTWARE.
--
--------------------------------------------------------------------------------
--
-- Starting points for zone offsets and mapsize taken from Nevir27's work on
-- LibSurveyor, all other data and code is original
--
--------------------------------------------------------------------------------
-- File:      Source/MapMonster_Pins.lua
-- Date:      2012-08-17T12:40:42Z
-- Author:    Philosound
-- Version:   v0.96
-- Revision:  123
-- File Rev:  123
-- Copyright: 2008
--------------------------------------------------------------------------------
if not MapMonster or not MapMonster.CoreLoaded then return end --shortcut loading the file if the core didnt load

local pairs = pairs

local LibToolkit = LibStub("LibToolkit-0.1")
--------------------------------------------------------------------------------
--#
--#			Local Definitions
--#
--------------------------------------------------------------------------------
local DefaultPin = { -- pin data
					 id        = 0,
					 pinType   = "",
					 subType   = "",
					 label     = L"New MapMonster Pin",
					 note      = {L"Created by MapMonster"},
					 isLocked  = false,
					 isPrivate = false,
					 -- position data
					 zoneId   = 0,
					 worldX   = 0,
					 worldY   = 0,
					 zoneX    = 0,
					 zoneY    = 0, }

local DefaultPinTypeOptions = {
					 label = L"No Label set",
					 defaultSubType = "",
					 radius = 1500,    -- distance from existing pin to be eligible to merge
					 isLocked = false, -- default lock status for new pins of this type
					 isPrivate = false, -- default private status for new pins of this type
					 mapIcon = { texture = "EA_HUD_01", -- default icon for map
								 scale = 0.65,
								 slice = "RvR-Flag",
								 -- Can also specify a tint as follows, which happen to match
								 -- any internal color definitions such as DefaultColor.RED
								 -- tint = { r = 0,
								 -- 		 g = 0,
								 --			 b = 0, }
								},
					 mouseClickCallback = "MapMonster.Editor.OpenViewWindow",
					 editCallback = "",
					 deleteCallback = "",
					 mergeHandler = "",
					 }

local DefaultCommonType = "MapMonster"
local DefaultCharacterType = "MapMonsterCharacter"

local DefaultPinTypes = {}
DefaultPinTypes[DefaultCommonType]    = { label = L"Map Monster" }
DefaultPinTypes[DefaultCharacterType] = { label = L"Map Monster - Character", defaultSubType = false }

local DefaultSubTypes = {}
DefaultSubTypes[DefaultCommonType] = {}
DefaultSubTypes[DefaultCommonType]["WPBlue"]   = { label = L"Waypoint - Blue",   mapIcon = { texture = "map_markers01", scale = 0.875, slice = "Waypoint-Large", tint = DefaultColor.BLUE, }, }
DefaultSubTypes[DefaultCommonType]["WPRed"]    = { label = L"Waypoint - Red",    mapIcon = { texture = "map_markers01", scale = 0.875, slice = "Waypoint-Large", tint = DefaultColor.RED, }, }
DefaultSubTypes[DefaultCommonType]["WPGreen"]  = { label = L"Waypoint - Green",  mapIcon = { texture = "map_markers01", scale = 0.875, slice = "Waypoint-Large", tint = DefaultColor.GREEN, }, }
DefaultSubTypes[DefaultCommonType]["WPPurple"] = { label = L"Waypoint - Purple", mapIcon = { texture = "map_markers01", scale = 0.875, slice = "Waypoint-Large", tint = DefaultColor.PURPLE, }, }
DefaultSubTypes[DefaultCommonType]["WPGold"]   = { label = L"Waypoint - Gold",   mapIcon = { texture = "map_markers01", scale = 0.875, slice = "Waypoint-Large", tint = DefaultColor.GOLD, }, }

--DefaultSubTypes[DefaultCharacterType] = {}

local DisplayedMapPins = {["EA_Window_WorldMapZoneViewMapDisplay"] = {},}
local MapPinCache = {["EA_Window_WorldMapZoneViewMapDisplay"] = {},}

local hookShowWorldMap
local hookShowZone
local hookOnClickMap
local hookShutdownPlayInterface
local lastZoneDisplayed = {["EA_Window_WorldMapZoneViewMapDisplay"] = 0,}
local forceZoneRefresh = false

local PinStorage = {}
local PinTypes = {}
local LastPinId = 0

local FilterMenus = {}  -- Table of Filter menus and sub menus
local FilterMenuWindows = {} -- Table of windows created for filter menu
local lastFilterMenuOpened = 0
local Filter_MAIN_MENU = 1
local Filter_SUB_MENU = 2

local dangerWillRobinson = true

local pinOnTop = 0
local HighlightWindowName = "MapMonsterHighlight"
local lastMapPinHighlight = ""

-- Load the lib to enable timestamps
local LibDateTime = LibStub("LibDateTime-Modified-0.4")

--------------------------------------------------------------------------------
--#
--#			Local Functions
--#
--------------------------------------------------------------------------------
local function GetDatestamp(datestamp)
	local dateString
	if LibDateTime then
		if datestamp then
			datestamp = LibDateTime:Parse(datestamp)
		else
			datestamp = LibDateTime:Now()
		end
		dateString = LibDateTime:Format(datestamp, "%Y-%m-%d %H:%M:%S")
	else
		dateString = "Unknown"
	end
	
	return dateString
end

--------------------------------------------------------------------------------
--# Validate the pin value, make sure its the right key and type
--#
--------------------------------------------------------------------------------
local function ValidatePinValue(key, value)
	if ( DefaultPin[key] == nil ) then
		MapMonster.ERROR_CODE = 301
		return false
	else
		local valType = type(DefaultPin[key])
		local newValType = type(value)
		if ( valType ~= newValType ) then
			MapMonster.ERROR_CODE = 302
			return false
		end
		return true
	end
end

--------------------------------------------------------------------------------
--#	Validate the pin options value, make sure its the right key and type
--#
--------------------------------------------------------------------------------
local function ValidateOptionValue(key, value)
	if ( DefaultPinTypeOptions[key] == nil or key == "subTypes") then
		MapMonster.ERROR_CODE = 303
		return false
	else
		local valType = type(DefaultPinTypeOptions[key])
		local newValType = type(value)
		d("Val Types: " .. valType .. ", " .. newValType)
		if ( valType ~= newValType ) then
			MapMonster.ERROR_CODE = 304
			return false
		end
		if ( key == "mapIcon") then
			if ( value.texture == nil or
				 value.scale == nil or
				 type(value.texture) ~= "string" or 
				 type(value.scale) ~= "number" )
			then
				MapMonster.ERROR_CODE = 205
				return false
			end
			if ( value.slice ~= nil and type(value.slice) ~= "string" ) then
				MapMonster.ERROR_CODE = 305
				return false
			end
			if ( value.tint ~= nil ) then
				if ( type(value.tint) ~= "table" or 
					 value.tint.r == nil or 
					 value.tint.g == nil or 
					 value.tint.b == nil)
				then
					MapMonster.ERROR_CODE = 306
					return false
				end
			end
		end
		return true
	end
end

--------------------------------------------------------------------------------
--# Internal GetPin to get a reference to the real pin
--#
--------------------------------------------------------------------------------
local function GetPin(pinId)
	
	if PinStorage[pinId] then
		return PinStorage[pinId]
	else
		MapMonster.ERROR_CODE = 105
		return false
	end
	
end

--------------------------------------------------------------------------------
--# Internal GetPinsForZone to get references instead of copies
--#
--------------------------------------------------------------------------------
local function GetPinsForZone(zoneId)

	local matchingPins = {}

	-- TODO: fix this clumsy linear search for pins in zone
	for pinId, pinData in pairs(PinStorage) do
		if ( pinData.zoneId == zoneId ) then
			matchingPins[pinId] = PinStorage[pinId]
		end
	end

	return matchingPins

end

--------------------------------------------------------------------------------
--# Internal version of GetPinsForZoneBySubType to return references and not copies
--#
--------------------------------------------------------------------------------
local function GetPinsForZoneBySubType(zoneId, pinType, subType, unlockedOnly)

	local matchingPins = {}

	-- TODO: fix this clumsy linear search for similar pins
	for pinId, pinData in pairs(PinStorage) do
		if ( pinData.zoneId    == zoneId  and 
		     pinData.pinType   == pinType and 
		     pinData.subType   == subType ) 
		then
			if ( not unlockedOnly or 
				 ( unlockedOnly and pinData.isLocked == false ) )
			then
				table.insert( matchingPins, PinStorage[pinId] )
			end
		end
	end

	return matchingPins

end

--------------------------------------------------------------------------------
--# Utility function to get a refence to a global value, used with callbacks
--#
--------------------------------------------------------------------------------
local GetGlobalValue = LibToolkit.GetGlobal

--------------------------------------------------------------------------------
--#
--#			Local Pin Display Functions
--#
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
--#	Show the flashy bits for a map pin
--#
--#	Parameters:
--#		pinId -(number) Map Monster Pin to display
--#		mapDisplay - (string) optional; defaults to world map
--------------------------------------------------------------------------------
local function FlashMapPin(pinId, mapDisplay)
	if not mapDisplay then mapDisplay = "EA_Window_WorldMapZoneViewMapDisplay" end
	lastMapPinHighlight = mapDisplay .. "MapMonster_MapPin" .. pinId

    WindowClearAnchors( HighlightWindowName ) 
    WindowAddAnchor( HighlightWindowName, "center", lastMapPinHighlight, "center", 0, -2 )
	WindowSetShowing( HighlightWindowName, true )
	AnimatedImageStartAnimation( HighlightWindowName, 1, true, false, 0)

	WindowSetShowing(lastMapPinHighlight, true)
	
end

--------------------------------------------------------------------------------
--#	Destory the map icon on the zone map
--#
--------------------------------------------------------------------------------
local function DestroyPinIcon(iconId, mapDisplay)
	if not mapDisplay then
		for aMapDisplay,_ in pairs(MapMonster.MainMapDisplays) do
			DestroyPinIcon(iconId, aMapDisplay)
		end
		return
	end
	local pinId = tonumber(string.match(iconId, "%d+"))

	local windowName = mapDisplay.."MapMonster_MapPin" .. pinId
	if DoesWindowExist(windowName) then
		DestroyWindow(windowName)
	end
	DisplayedMapPins[mapDisplay][pinId] = nil
end

--------------------------------------------------------------------------------
--# Destory all the map icons on the zone map
--#
--------------------------------------------------------------------------------
local function ClearPins(mapDisplay)
	if not mapDisplay then
		for aMapDisplay,_ in pairs(MapMonster.MainMapDisplays) do
			ClearPins(aMapDisplay)
		end
		return
	end
	
	for pinId, _ in pairs(DisplayedMapPins[mapDisplay]) do
		DestroyPinIcon(pinId, mapDisplay)
	end
end

--------------------------------------------------------------------------------
--# Get the icon details to use for a map icon
--#
--------------------------------------------------------------------------------
local function GetMapIcon(pinType, subType)

	local iconInfo

	if ( PinTypes[pinType].subTypes[subType].mapIcon ~= nil ) then
		iconInfo = PinTypes[pinType].subTypes[subType].mapIcon
	elseif ( PinTypes[pinType].mapIcon ~= nil ) then
		iconInfo = PinTypes[pinType].mapIcon
	else
		MapMonster.ERROR_CODE = 106
		return false
	end

	return iconInfo

end

--------------------------------------------------------------------------------
--# Display the map icon on the zone map for pinId
--#
--------------------------------------------------------------------------------
local function DisplayPin(pinId, mapDisplay)
	if not mapDisplay then
		for aMapDisplay,_ in pairs(MapMonster.MainMapDisplays) do
			DisplayPin(pinId, aMapDisplay)
		end
		return
	end	
	local pin
	-- if the pinId is a number get the pin, if we got the whole pin data use it
	if type(pinId) == "number" then
		pin = GetPin(pinId)
	elseif type(pinId) == "table" then
		pin = pinId
	end

	--[===[@alpha@
	--d("Display Pin: " .. pin.id)
	--@end-alpha@]===]

	-- filter pins displayed
	local filterKey = pin.pinType .. ":" .. pin.subType
	if ( not MM_Settings.Filters[pin.pinType] or not MM_Settings.Filters[filterKey] ) then
		-- dont display the icon
		return false
	end
	-- On the off chance we have world but no zone don't try and display it
	if pin.zoneX == nil or pin.zoneY == nil then
		return false
	end

	-- get the mapsize in coords
	local zoneInfo = MapMonsterAPI.GetZoneData(pin.zoneId)
	if not zoneInfo or zoneInfo.noMap then return false end
	
	--local windowId = table.getn(DisplayedMapPins)
	local mapIcon = GetMapIcon(pin.pinType, pin.subType)

	local newMapIconName = mapDisplay.."MapMonster_MapPin" .. pin.id
	CreateWindowFromTemplate(newMapIconName, "MapMonster_MapPin", mapDisplay)
	WindowSetId(newMapIconName, pin.id)
	
	-- TODO: this might not work for ability icons that need x and y
	DynamicImageSetTexture(newMapIconName, mapIcon.texture, mapIcon.x or 0, mapIcon.y or 0)
	DynamicImageSetTextureScale(newMapIconName, mapIcon.scale)
	if mapIcon.slice then DynamicImageSetTextureSlice(newMapIconName, mapIcon.slice) end
	if mapIcon.tint then
		WindowSetTintColor(newMapIconName, mapIcon.tint.r, mapIcon.tint.g, mapIcon.tint.b)
	else
		WindowSetTintColor(newMapIconName, 255, 255, 255)
	end

	-- get the window dimensions to scale
	if mapDisplay ~= "MinmapMapDisplay" then
		local windowX, windowY = WindowGetDimensions(mapDisplay) -- 1000, 1000

		local xPosUnit = (windowX / zoneInfo.mapsizeX) + (zoneInfo.mapStartOffsetX or 0)
		local yPosUnit = (windowY / zoneInfo.mapsizeY) + (zoneInfo.mapStartOffsetY or 0)

		local anchorX = pin.zoneX * xPosUnit
		local anchorY = pin.zoneY * yPosUnit

		WindowClearAnchors(newMapIconName)
		WindowAddAnchor(newMapIconName, "topleft", mapDisplay, "center", anchorX, anchorY)
		WindowSetScale(newMapIconName, 0.8 * WindowGetScale(mapDisplay))
		WindowSetShowing(newMapIconName, true)
		DisplayedMapPins[mapDisplay][pin.id] = { x = anchorX, y = anchorY }
	else	-- try next gen
		local anchorX, anchorY
		if zoneInfo.chunkX then
			local function _trimWorldPos(oldPos, maxPos)
				local newPos = oldPos
				while newPos > maxPos do
					newPos = newPos - maxPos
				end
				return newPos
			end
			anchorX, anchorY = MapGetPointForCoordinates("MinmapMapDisplay", _trimWorldPos(pin.worldX, zoneInfo.chunkX), _trimWorldPos(pin.worldY, zoneInfo.chunkY))
		else
			anchorX, anchorY = MapGetPointForCoordinates("MinmapMapDisplay", pin.worldX, pin.worldY)
		end
		WindowClearAnchors(newMapIconName)
		WindowAddAnchor(newMapIconName, "topleft", mapDisplay, "center", anchorX, anchorY)
		WindowSetScale(newMapIconName, 1.25 * MinmapSettings.worldmapscale * WindowGetScale(mapDisplay))
		WindowSetShowing(newMapIconName, true)
		DisplayedMapPins[mapDisplay][pin.id] = { x = anchorX, y = anchorY }		
	end
	
	return true
end

--------------------------------------------------------------------------------
--# Destory and re-create the map icon
--#
--------------------------------------------------------------------------------
local function UpdatePinIcon(pinId)

	--[===[@alpha@
	d("Update Pin: " .. pinId)
	--@end-alpha@]===]
	for mapDisplay,_ in pairs(MapMonster.MainMapDisplays) do
		if DisplayedMapPins[mapDisplay][pinId] then
			DestroyPinIcon(pinId, mapDisplay)
			DisplayPin(pinId, mapDisplay)
		end
	end
end

--------------------------------------------------------------------------------
--# Display all the map icons for specified zone
--#
--------------------------------------------------------------------------------
local function DisplayPinsForZone(zoneId, mapDisplay)
	if not mapDisplay then mapDisplay = "EA_Window_WorldMapZoneViewMapDisplay" end

	-- clear highlight
	if WindowGetShowing(HighlightWindowName) then
		WindowSetShowing(HighlightWindowName, false)
		WindowSetParent(HighlightWindowName, mapDisplay)
	end

	ClearPins(mapDisplay)

	if zoneId ~= lastZoneDisplayed[mapDisplay] or forceZoneRefresh then
		MapPinCache[mapDisplay] = GetPinsForZone(zoneId)
		lastZoneDisplayed[mapDisplay] = zoneId
		forceZoneRefresh = false
	end
	
	local popPin = false
	for i, pinData in pairs(MapPinCache[mapDisplay]) do
		DisplayPin(pinData, mapDisplay)
		if pinData.id == pinOnTop then
			popPin = true
		end
	end

	if popPin then
		WindowSetShowing(mapDisplay .. "MapMonster_MapPin" .. pinOnTop, true)
	end
	
end

--------------------------------------------------------------------------------
--#
--#			Local Pin Data Functions
--#
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
--# Get the callback to use for pin action type
--#
--------------------------------------------------------------------------------
local function GetCallback(pinType, subType, callbackType)

	local callback

	if ( callbackType == "mouseclick" ) then
		callbackType = "mouseClickCallback"
	elseif ( callbackType == "edit" ) then
		callbackType = "editCallback"
	elseif ( callbackType == "delete" ) then
		callbackType = "deleteCallback"
	elseif ( callbackType == "merge" ) then
		callbackType = "mergeHandler"
	else
		callbackType = "none"
	end

	if (PinTypes[pinType].subTypes[subType][callbackType] ~= nil and 
	    PinTypes[pinType].subTypes[subType][callbackType] ~= "" )
	then
		callback = PinTypes[pinType].subTypes[subType][callbackType]
	elseif (PinTypes[pinType][callbackType] ~= nil and 
        PinTypes[pinType][callbackType] ~= "" )
	then
		callback = PinTypes[pinType][callbackType]
	else
		if callbackType == "mouseClickCallback" then
			callback = DefaultPinTypeOptions[callbackType]
		else
			return false
		end
	end

	callback = GetGlobalValue(callback)
	if (type(callback) ~= "function") then
		return false
	else
		return callback
	end

end

--------------------------------------------------------------------------------
--# Merge two pins together, falls back to saving a new pin
--#
--------------------------------------------------------------------------------
local function MergePin(newPin, existPin)

	--[===[@alpha@
	d("Merge Pin: " .. existPin.id)
	--@end-alpha@]===]
	
	if (newPin == nil or existPin == nil or type(newPin) ~= "table" or type(existPin) ~= "table") then
		MapMonster.ERROR_CODE = 206
		return false
	end
	
	local newPin = LibToolkit.CopyObject(newPin)
	local mergedPin = LibToolkit.CopyObject(existPin)

	-- merge positions
	mergedPin.worldX = LibToolkit.roundNum( ( existPin.worldX + newPin.worldX ) / 2 )
	mergedPin.worldY = LibToolkit.roundNum( ( existPin.worldY + newPin.worldY ) / 2 )
	-- get new zone pos

	local zonePos = MapMonsterAPI.WorldToZone(mergedPin)

	if zonePos then
		mergedPin.zoneX = zonePos.zoneX
		mergedPin.zoneY = zonePos.zoneY
	else
		d(MapMonsterAPI.GetError())
		mergedPin.zoneX = nil
		mergedPin.zoneY = nil
	end

	-- delete duplicate notes
	for _, oldNote in ipairs(mergedPin.note) do
		for i, newNote in ipairs(newPin.note) do
			if (oldNote == newNote) then
				newPin.note[i] = nil -- delete identical note
			end
		end
	end
	-- add any remaining notes to existing pin
	for i, newNote in ipairs(newPin.note) do
		table.insert(mergedPin.note, newNote)
	end

	-- set new datestamp for the modified pin
	mergedPin.modified  = GetDatestamp()

	-- if the subtype or pintype has a merge callback use it
	local callback = GetCallback(mergedPin.pinType, mergedPin.subType, "merge")
	if callback then
		-- fire off the callback, if it fails or we get false fall back to save
		local success, mergedData = pcall(callback, newPin.Data, existPin.Data)
		if not success or ( success and mergedData == false ) then
			return false -- if we got false from the addon callback or an error then kill the merging
		else
			mergedPin.Data = mergedData
		end
	end
	
	PinStorage[mergedPin.id] = mergedPin

	return mergedPin.id

end

--------------------------------------------------------------------------------
--# Saves a new pin or merges two pins together, falls back to saving
--#
--------------------------------------------------------------------------------
local function SavePin(mapPin, merge)

	--[===[@alpha@
	d("Save Pin")
	--@end-alpha@]===]

	local pinId = false
	
	-- we want to merge this pin with any nearby pins
	if merge and mapPin.isLocked == false then
		local nearbyPins = GetPinsForZoneBySubType(mapPin.zoneId, mapPin.pinType, mapPin.subType, true)
		if not nearbyPins then
			return SavePin(mapPin, false) -- nothing to merge with so save it
		end
		local mergeRadius = MapMonsterAPI.GetPinTypeOption(mapPin.pinType, mapPin.subType, "radius")
		-- check distance and label
		for i, pinData in pairs(nearbyPins) do
			-- if the labels match check distance
			if (pinData.label == mapPin.label) then
				local distance = MapMonsterAPI.GetDistance( mapPin, pinData )
				-- if its within the merge radius
				if (distance <= mergeRadius) then
					pinId = MergePin(mapPin, pinData)
					if ( pinId == false) then
						return SavePin(mapPin, false) -- the merge failed so save it
					else
						break -- found a pin and merge went well
					end
				end
			end
		end
		-- If we found nearby pins, even some to merge but they were too far or failed
		if not pinId then
			return SavePin(mapPin, false) -- the merge failed so save it
		end

	-- just save the pin we dont want to merge it with any nearby pins
	else
		-- get pin id
		pinId = LastPinId + 1
		-- set the pin id on the new pin
		mapPin.id = pinId
		-- Add the pin to our table
		PinStorage[pinId] = mapPin
		-- save the last pinId
		LastPinId = pinId
	end

	-- if this pin is in the last displayed zone update the display
	for aMapDisplay,_ in pairs(MapMonster.MainMapDisplays) do		
		if ( mapPin.zoneId == lastZoneDisplayed[aMapDisplay] ) then
			if merge then
				UpdatePinIcon(pinId)
			else
				DisplayPin(pinId, aMapDisplay)
				MapPinCache[aMapDisplay][pinId] = PinStorage[pinId]
			end
		end
	end
	
	return pinId

end

--------------------------------------------------------------------------------
--# Verify map pins and zone data version to make sure everything in storage
--# is up to date, can run any conversion or migration needed here
--#
--------------------------------------------------------------------------------
local function ValidateMapPins()

	--[===[@alpha@
	d("Validating Map Pins in storage")
	--@end-alpha@]===]
	
	if MM_Settings.ZoneData_VERSION == nil then MM_Settings.ZoneData_VERSION = MapMonster.ZoneData_VERSION end

	if MM_Settings.ZoneData_VERSION ~= MapMonster.ZoneData_VERSION then
		-- This is where I stick any data migration or adjustment when basic zone data changes
		-- and we need to modify the pins to match the new info
	end

end

--------------------------------------------------------------------------------
--#
--#			Local UI Functions
--#
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
--# Saves and restores default Warhammer map filters for the zone map toggle
--#
--------------------------------------------------------------------------------
local function ToggleWHMapFilters(state, skipSave)

	if MM_Settings.WHMapFilters == nil then
		MM_Settings.WHMapFilters = {}
	end

	-- toggle map filters
    for index, category in ipairs( EA_Window_WorldMap.filterCategories ) do
		local showMapIcon
		if state == true then -- turn filters back on
			if not MM_Settings.WHMapFilters[index] then
				MM_Settings.WHMapFilters[index] = true -- default filters on
			end
			showMapIcon = MM_Settings.WHMapFilters[index]
		else -- turn filters off
			if not skipSave then
				MM_Settings.WHMapFilters[index] = EA_Window_WorldMap.Settings.filterCategories[index]
			end
			showMapIcon = false
		end

        local windowName  = "EA_Window_WorldMapZoneViewMapFilter"..index

        -- Button State
        ButtonSetPressedFlag( windowName.."CheckBox", showMapIcon )
        
        -- Set the Map Filters
        for _, filterType in pairs( category.filterTypes ) do        
            MapSetPinFilter("EA_Window_WorldMapZoneViewMapDisplay", filterType, showMapIcon )
        end              
    end

end

--------------------------------------------------------------------------------
--# Saves and restores MapMonster pin filters for the zone map toggle button
--#
--------------------------------------------------------------------------------
local function ToggleMapMonsterFilters(state, skipSave)

	if MM_Settings.MonsterFilters == nil then
		MM_Settings.MonsterFilters = {}
	end

	-- toggle map filters
    for pinType, filtered in pairs( MM_Settings.Filters ) do

		if state == true then -- turn filters back on
			MM_Settings.Filters[pinType] = MM_Settings.MonsterFilters[pinType]
		else -- turn filters off
			if not skipSave then
				MM_Settings.MonsterFilters[pinType] = MM_Settings.Filters[pinType]
			end
			MM_Settings.Filters[pinType] = false
		end

		local windowName  = string.gsub(pinType, ":", "_")
		windowName = "MapFilterChoice_" .. windowName .. "CheckBox"

		-- Button State
		if DoesWindowExist(windowName) then
			ButtonSetPressedFlag( windowName, MM_Settings.Filters[pinType] )
		else
			MM_Settings.Filters[pinType] = nil
		end

    end

	DisplayPinsForZone(lastZoneDisplayed["EA_Window_WorldMapZoneViewMapDisplay"])
end

--------------------------------------------------------------------------------
--# Sets the filters and button text for specified state
--#
--------------------------------------------------------------------------------
local function FilterButtonState(stateId, skipSave)

	if stateId == MapMonster.SHOW_ALL then
		MM_Settings.CycleSetting = MapMonster.SHOW_ALL
		ButtonSetText("MapMonster_FilterButton", L"All Map Icons") 		
		ToggleWHMapFilters(true, skipSave)
	elseif stateId == MapMonster.SHOW_DEFAULT then
		MM_Settings.CycleSetting = MapMonster.SHOW_DEFAULT
		ButtonSetText("MapMonster_FilterButton", L"Default Icons")
		ToggleMapMonsterFilters(false, skipSave)
	elseif stateId == MapMonster.SHOW_CUSTOM then
		MM_Settings.CycleSetting = MapMonster.SHOW_CUSTOM
		ButtonSetText("MapMonster_FilterButton", L"MapMonster")
		ToggleWHMapFilters(false, skipSave)
		ToggleMapMonsterFilters(true, skipSave)
	end

end

--------------------------------------------------------------------------------
--# Creates the menu item used to build the right click context menu
--#
--------------------------------------------------------------------------------
local function CreateFilterMenuEntry(entryData)

	if entryData.subMenu then
		CreateWindowFromTemplate( entryData.windowName, "MapFilterContextMenuChoice", "Root" )
	else
		CreateWindowFromTemplate( entryData.windowName, "MapFilterContextSubMenuChoice", "Root" )
	end

	LabelSetText( entryData.windowName .."Text", entryData.label )

	local labelX, labelY = LabelGetTextDimensions( entryData.windowName .. "Text" )
	WindowSetDimensions( entryData.windowName, labelX + 87, 28)
	-- Bug fix for label 
	WindowSetDimensions( entryData.windowName .. "Text", labelX + 10, 32)
	LabelSetText( entryData.windowName .."Text", entryData.label )

	DynamicImageSetTexture( entryData.windowName .. "MapIcon" , entryData.mapIcon.texture, 0, 0)
	DynamicImageSetTextureScale( entryData.windowName .. "MapIcon" , entryData.mapIcon.scale)
	if entryData.mapIcon.slice then
		DynamicImageSetTextureSlice( entryData.windowName .. "MapIcon" , entryData.mapIcon.slice)
	end
	if entryData.mapIcon.tint then
		WindowSetTintColor( entryData.windowName .. "MapIcon" , entryData.mapIcon.tint.r, entryData.mapIcon.tint.g, entryData.mapIcon.tint.b)
	else
		WindowSetTintColor( entryData.windowName .. "MapIcon" , 255, 255, 255)
	end
	
	WindowSetId( entryData.windowName.."CheckBox", entryData.windowId  )
	ButtonSetCheckButtonFlag( entryData.windowName.."CheckBox", true)
	WindowSetShowing( entryData.windowName.."CheckBox", true )
	WindowSetShowing( entryData.windowName.."SubMenuButton", (entryData.subMenu ~= nil))

	WindowSetShowing( entryData.windowName, false)
	table.insert(FilterMenuWindows, entryData.windowName)

end

--------------------------------------------------------------------------------
--# Destory the menu items used for the context menu
--#
--------------------------------------------------------------------------------
local function DestroyPinFilterMenus()

	for _, windowName in pairs(FilterMenuWindows) do
		DestroyWindow(windowName)
	end
	FilterMenus = {}
	FilterMenuWindows = {}

end

--------------------------------------------------------------------------------
--# Assembles the composite window id to identify the menu item and pin type
--#
--------------------------------------------------------------------------------
local function GetCompositeWindowId(mainIndex, subIndex)

	local mainIndex = mainIndex or 0
	local subIndex = subIndex or 0

	return (mainIndex * 100) + subIndex

end

--------------------------------------------------------------------------------
--# Splits up the windowId to get the pin type and menu indexes
--#
--------------------------------------------------------------------------------
local function GetFilterMenuIndexes(windowId)

	local subIndex = windowId % 100
	local mainIndex = math.floor( (windowId - subIndex) / 100)

	return mainIndex, subIndex

end

--------------------------------------------------------------------------------
--# Shows the main menu for filter context menu
--#
--------------------------------------------------------------------------------
local function ShowFilterMenu( menuIndex, contextMenuID )

	lastFilterMenuOpened = menuIndex
	local anchorWindow = SystemData.MouseOverWindow.name
	EA_Window_ContextMenu.CreateContextMenu( anchorWindow, contextMenuID )

	for filterIndex, filterData in pairs( FilterMenus[menuIndex] ) do
		EA_Window_ContextMenu.AddUserDefinedMenuItem(filterData.windowName, contextMenuID)
		ButtonSetPressedFlag( filterData.windowName.."CheckBox", MM_Settings.Filters[filterData.filterKey] )
	end
	
    EA_Window_ContextMenu.Finalize(contextMenuID)

end

--------------------------------------------------------------------------------
--# Shows a submenu for filter context menu
--#
--------------------------------------------------------------------------------
local function ShowFiltersSubMenu()

	local menuIndex = WindowGetId( SystemData.MouseOverWindow.name )
	local menuData = FilterMenus[lastFilterMenuOpened][menuIndex]

	if menuData.subMenu ~= nil then 
		ShowFilterMenu( menuData.subMenu, Filter_SUB_MENU )
	end
	
end

--------------------------------------------------------------------------------
--# Hides all the context menu windows
--#
--------------------------------------------------------------------------------
local function HideFilterMenus()
	lastFilterMenuOpened = 0	
	EA_Window_ContextMenu.HideAll()
end

--------------------------------------------------------------------------------
--# Hides context sub menu
--#
--------------------------------------------------------------------------------
local function HideLastFilterSubMenus()
	lastFilterMenuOpened = Filter_MAIN_MENU
	EA_Window_ContextMenu.Hide( Filter_SUB_MENU )
end

--------------------------------------------------------------------------------
--# Builds the menus and submenus for filters context menu from scratch
--#
--------------------------------------------------------------------------------
local function InitializeFilters()

	--[===[@alpha@
	d("Initialize Map Pin Filters")
	--@end-alpha@]===]
	DestroyPinFilterMenus()

	if MM_Settings.Filters == nil then
		MM_Settings.Filters = {}
	end

	-- generate filter main menu table
	local mainMenu = {}
	for pinType, typeOptions in pairs(PinTypes) do
		if not LibToolkit.IsTableEmpty(typeOptions.subTypes) then
			local filterEntry = { label = typeOptions.label,
								  filterKey = pinType,
			              		  windowName = "MapFilterChoice_" .. pinType,
			          		      windowId = 0,
		  	    		          subMenu = 0,
								  mapIcon = typeOptions.mapIcon, }
			table.insert(mainMenu, filterEntry)
			if MM_Settings.Filters[filterEntry.filterKey] == nil then
				MM_Settings.Filters[filterEntry.filterKey] = true
			end
		end
	end
	table.sort(mainMenu, MapMonster.CustomSortByLabel)
	table.insert(FilterMenus, mainMenu)

	-- generate submenus
	for k, menuEntry in pairs(FilterMenus[1]) do
		local subMenu = {}
		for subType, subOptions in pairs(PinTypes[menuEntry.filterKey].subTypes) do
			local filterEntry = { label = subOptions.label,
								  filterKey = menuEntry.filterKey .. ":" .. subType,
								  windowName = "MapFilterChoice_" .. menuEntry.filterKey .. "_" .. subType,
								  windowId = 0,
								  }
			if subOptions.mapIcon then
				filterEntry.mapIcon = subOptions.mapIcon
			else
				filterEntry.mapIcon = PinTypes[menuEntry.filterKey].mapIcon
			end
			table.insert(subMenu, filterEntry)
			if MM_Settings.Filters[filterEntry.filterKey] == nil then
				MM_Settings.Filters[filterEntry.filterKey] = true
			end
		end
		table.sort(subMenu, MapMonster.CustomSortByLabel)
		table.insert(FilterMenus, subMenu)
		local subMenuId = table.getn(FilterMenus)
		menuEntry.subMenu = subMenuId
	end

	-- assign windowIds and Create windows
	for menuId, menuInfo in pairs(FilterMenus) do
		local maxX = 0
		local maxY = 0
		for itemId, menuItem in pairs(menuInfo) do
			local windowId = GetCompositeWindowId(menuId, itemId)
			menuItem.windowId = windowId
			CreateFilterMenuEntry(menuItem)
			local windowX, windowY = WindowGetDimensions(menuItem.windowName)
			if windowX > maxX then
				maxX = windowX
			end
			if windowY > maxY then
				maxY = windowY
			end
		end
		-- resize menu items to match longest
		for itemId, menuItem in pairs(menuInfo) do
			WindowSetDimensions(menuItem.windowName, maxX, maxY)
		end
	end

	-- fix the editor combo boxes for the new types if the editor is built
	if DoesWindowExist("MapMonster_EditorWindow") then
		MapMonster.Editor.BuildPinTypeComboBox()
	end
end

--------------------------------------------------------------------------------
--#
--#			Global MapMonster Pin Event Handlers and UI Functions
--#
--------------------------------------------------------------------------------
MapMonster.PinFileLoaded = true

MapMonster.SHOW_ALL = 0
MapMonster.SHOW_DEFAULT = 1
MapMonster.SHOW_CUSTOM = 2

MapMonster.GENERAL_PINTYPE = DefaultCommonType
MapMonster.CHARACTER_PINTYPE = DefaultCharacterType

--------------------------------------------------------------------------------
--#	Initialize and setup of Map Pins , called from MapMonster.Initialize
--#
--------------------------------------------------------------------------------
function MapMonster.InitializeMapPins()

	--[===[@alpha@
	d("Initialize MapMonster Pins")
	--@end-alpha@]===]
	-- Initialize Saved Variables we'll need
	if not MapMonster.PinStorage then MapMonster.PinStorage = {} end
	if (MM_PinStorage and next(MM_PinStorage) ~= nil) then	-- check for the old vars
		for k,v in pairs(MM_PinStorage) do
			MapMonster.PinStorage[k] = v
		end
		MM_PinStorage = nil
	end
	if not MapMonster.PinTypes then MapMonster.PinTypes = {} end
	if (MM_PinTypes and next(MM_PinTypes) ~= nil) then
		for k,v in pairs(MM_PinTypes) do
			MapMonster.PinTypes[k] = v
		end
		MM_PinTypes = nil
	end
	if (MM_Settings == nil) then
		-- check for the old vars
		if MapMonster.Settings then
			MM_Settings = MapMonster.Settings
			MapMonster.Settings = nil
		else
			MM_Settings = { CycleSetting = 0,
							Filters = {},
							WHMapFilters = {},
							MonsterFilters = {},
							ZoneData_VERSION = MapMonster.ZoneData_VERSION,
							FlashColor = DefaultColor.YELLOW,
						}
		end
	end

	if not MM_Settings.FlashColor then MM_Settings.FlashColor = DefaultColor.YELLOW end
	if not MM_Settings.LastUserType then MM_Settings.LastUserType = 1 end

	PinStorage = MapMonster.PinStorage
	PinTypes = MapMonster.PinTypes
	LastPinId = MM_Settings.LastPinId or table.maxn(PinStorage)
	
	ValidateMapPins()
	InitializeFilters()		

	-- Init the flashy marker to map pins
	CreateWindowFromTemplate( HighlightWindowName , "AnimatedHighlight", "EA_Window_WorldMapZoneViewMapDisplay")
	WindowSetShowing( HighlightWindowName, false )
	WindowSetTintColor( HighlightWindowName, MM_Settings.FlashColor.r, MM_Settings.FlashColor.g, MM_Settings.FlashColor.b)

	-- Initialize zone map button
	CreateWindowFromTemplate("MapMonster_FilterButton", "MapMonster_FilterButton", "EA_Window_WorldMapZoneView")
	WindowSetShowing("MapMonster_FilterButton", true)

	FilterButtonState(MM_Settings.CycleSetting, true)

	--TODO: I can do this better with a table and a loop

	-- Check for the old player pin type and switch to the new one
	if MapMonsterAPI.PinTypeIsRegistered("MapMonsterPlayer") then
		-- ugly hack around the API to change the pintype without losing the pins
		local registeredPins = MapMonsterAPI.GetPinsByType("MapMonsterPlayer")
		if registeredPins then
			for k, pinData in pairs(registeredPins) do
				MapMonsterAPI.SetPinProperty(pinData.id, "pinType", "MapMonsterCharacter", false)
				MapMonsterAPI.SetPinProperty(pinData.id, "subType", string.gsub(pinData.subType, "%^%a", ""), false)
			end
		end
		-- copy the old player type to the new character type
		PinTypes["MapMonsterCharacter"] = PinTypes["MapMonsterPlayer"]
		PinTypes["MapMonsterCharacter"].id = PinTypes["MapMonsterCharacter"]
		PinTypes["MapMonsterCharacter"].label = L"Map Monster - Character"
		if PinTypes["MapMonsterCharacter"].defaultSubType == "MapMonsterPlayer" then
			PinTypes["MapMonsterCharacter"].defaultSubType = "MapMonsterCharacter"
		end
		--hack to fix bad character name subTypes
		local newSubTypes = {}
		for k, v in pairs(PinTypes["MapMonsterCharacter"].subTypes) do
			local newKey = string.gsub(k, "%^%a", "")
			local newRecord = {}
			newRecord.id = newKey
			newRecord.label = wstring.gsub(v.label, L"%^%a", L"")
			newSubTypes[newKey] = newRecord
		end
		PinTypes["MapMonsterCharacter"].subTypes = newSubTypes
	end
	
	-- Create default Pin Types
	for pinType, typeOptions in pairs(DefaultPinTypes) do
		local pinTypeOptions = LibToolkit.CopyObject(DefaultPinTypeOptions)
		local makeDefault = true
		if typeOptions.defaultSubType ~= nil then
			makeDefault = typeOptions.defaultSubType
			typeOptions.defaultSubType = nil
		end
		for k, v in pairs(typeOptions) do
			pinTypeOptions[k] = v
		end
		MapMonsterAPI.CreatePinType(pinType, pinTypeOptions, makeDefault)
	end

	-- Create default sub types
	for pinType, subTypes in pairs(DefaultSubTypes) do
		local pinTypeOptions = LibToolkit.CopyObject(DefaultPinTypeOptions)
		for subType, subOptions in pairs(subTypes) do
			for k, v in pairs(subOptions) do
				pinTypeOptions[k] = v
			end
			MapMonsterAPI.CreatePinSubType(pinType, subType, pinTypeOptions)
		end
	end
	
	-- create a per player subType here
	local playername =  LibToolkit.Clean.PlayerNameW
	local servername = LibToolkit.Clean.ServerNameW
	local pinTypeOptions = {}
	pinTypeOptions.label = playername .. L" - " .. servername
	MapMonsterAPI.CreatePinSubType(DefaultCharacterType, WStringToString(playername) .. string.gsub(WStringToString(servername), " ", "") , pinTypeOptions)

end

--------------------------------------------------------------------------------
--# Function to setup function hooks when loading was successfull
--# 
--------------------------------------------------------------------------------
function MapMonster.InitializeHooks()
	
	--[===[@alpha@
	d("Initialize Pin Hooks")
	--@end-alpha@]===]

	-- hook EA_Window_WorldMap.OnShown
	hookShowWorldMap = EA_Window_WorldMap.OnShown
	EA_Window_WorldMap.OnShown = MapMonster.ShowWorldMapHooked

	-- hook EA_Window_WorldMap.ShowZone
	hookShowZone = EA_Window_WorldMap.ShowZone
	EA_Window_WorldMap.ShowZone = MapMonster.ShowZoneHooked
	
	-- hook EA_Window_WorldMap.EA_Window_WorldMap.OnClickMap
	hookOnClickMap = EA_Window_WorldMap.OnClickMap
	EA_Window_WorldMap.OnClickMap = MapMonster.OnClickMapHooked

	-- setup map button down event -- Unused for the moment
	--WindowRegisterCoreEventHandler("EA_Window_WorldMapZoneViewMapDisplay", "OnLButtonDown", "MapMonster.OnLButtonDownMapDisplay")
end

--------------------------------------------------------------------------------
--# Function to shutdown this module if loading failed
--# 
--------------------------------------------------------------------------------
function MapMonster.ShutdownPins()
	-- destroy the zone map button
	if DoesWindowExist("MapMonster_FilterButton") then
		DestroyWindow("MapMonster_FilterButton")
	end
end


--------------------------------------------------------------------------------
--# Function hook into World Map OnShown, everytime the world map is shown
--# check for a forced refresh and refresh zone pins
--------------------------------------------------------------------------------
function MapMonster.ShowWorldMapHooked(...)
	--[===[@alpha@
	d("MM Show map hook")
	--@end-alpha@]===]
	hookShowWorldMap(...)
	if GameData.Player.zone ~= lastZoneDisplayed["EA_Window_WorldMapZoneViewMapDisplay"] or forceZoneRefresh then
		DisplayPinsForZone(GameData.Player.zone)
	end
end

--------------------------------------------------------------------------------
--# Function hook into ZoneMap ShowZone, everytime the zone is shown or refreshed
--# this will update the display of mappins along with it
--------------------------------------------------------------------------------
function MapMonster.ShowZoneHooked(...)	--zoneId
	hookShowZone(...)
	DisplayPinsForZone(...)
end

--------------------------------------------------------------------------------
--# Function hook into the ZoneMap click event to be able to use the ctrl+lbutton
--#
--------------------------------------------------------------------------------
function MapMonster.GetClickPos(mapDisplay, zoneId, flags, x, y)
	-- Get the human readbale map coords of the mouse Click
	if not (x and y) then
		local mapPositionX, mapPositionY = WindowGetScreenPosition( mapDisplay )
		x = SystemData.MousePosition.x - mapPositionX
		y = SystemData.MousePosition.y - mapPositionY
	end
	local resolutionScale = WindowGetScale(mapDisplay)	--InterfaceCore.GetResolutionScale() * 
	
	local mapX, mapY = MapGetCoordinatesForPoint( mapDisplay, x / resolutionScale, y / resolutionScale )
	return { zoneId = zoneId, zoneX = mapX or 0, zoneY = mapY or 0}
end
function MapMonster.DoClickMap(mapDisplay, zoneId, flags, x, y)
	if flags == (SystemData.ButtonFlags.CONTROL) then
		local clickPos = MapMonster.GetClickPos(mapDisplay, zoneId, flags, x, y)
		local cleanPos = MapMonsterAPI.ValidateZonePos(clickPos)
		if cleanPos then
			MapMonster.Editor.OpenAddWindow(cleanPos)
		else
			MapMonster.print("Cannot create pin in a zone with no map")
		end
		return true	-- "has handled Map Click"
	end
	return false
end
function MapMonster.OnClickMapHooked(flags, mapX, mapY)	--mapX == SystemData.MousePosition.x - WindowGetScreenPosition( mapDisplay )
	if( EA_Window_WorldMap.currentLevel == GameDefs.MapLevel.ZONE_MAP) then
		MapMonster.DoClickMap("EA_Window_WorldMapZoneViewMapDisplay", EA_Window_WorldMap.currentMap, flags, mapX, mapY)
	end
	return hookOnClickMap(flags, mapX, mapY)
end

--------------------------------------------------------------------------------
--# Function hook into ZoneMap Lbutton down event, not used right now
--#
--------------------------------------------------------------------------------
function MapMonster.OnLButtonDownMapDisplay(flags, mapX, mapY)
	-- Unused for the moment
	--[===[@alpha@
	d("On click MapDisplay button down")
	--@end-alpha@]===]
end

--------------------------------------------------------------------------------
--#	Set the highlight tint from saved settings
--#
--------------------------------------------------------------------------------
function MapMonster.HighlightSetTint()
	WindowSetTintColor( HighlightWindowName, MM_Settings.FlashColor.r, MM_Settings.FlashColor.g, MM_Settings.FlashColor.b)
end

--------------------------------------------------------------------------------
--# Event handler when mouse over map icons
--#
--------------------------------------------------------------------------------
function MapMonster.OnMouseOverPin()

    local windowName	= SystemData.ActiveWindow.name
	local parent = WindowGetParent(windowName)
	local pinId = WindowGetId(windowName)
	local pinData = GetPin(pinId)

	MapMonster.CreateMapPinTooltip(pinData)

	-- find nearby pins and those underneath
	local mouseOverAnchors = DisplayedMapPins[parent][pinId]
	local extraTooltipDisplay = {}
	local deltaX, deltaY, distance
	local index = 1
	for displayId, pinAnchors in pairs(DisplayedMapPins[parent]) do
		if displayId ~= pinId then
			-- check for proximity
			deltaX = math.abs(mouseOverAnchors.x - pinAnchors.x)
			deltaY = math.abs(mouseOverAnchors.y - pinAnchors.y)
			distance = math.sqrt( (deltaX * deltaX) + (deltaY * deltaY) )
			if distance < 25 then
			-- add as an extra to the tooltip
				index = index + 1
				MapMonster.SetExtraTooltipInfo(index, displayId)
			end
		end
	end
	
	MapMonster.ResizeTooltip()
	MapMonster.AnchorTooltip()
	
end

--------------------------------------------------------------------------------
--# Event handler for Lbutton click on map icon, calls pin callbacks to pass the 
--# click along to other addons
--#
--------------------------------------------------------------------------------
function MapMonster.OnMouseClickPin()

    local windowName	= SystemData.ActiveWindow.name
	local pinId = WindowGetId(windowName)
	local pinData = MapMonsterAPI.GetPin(pinId)

	-- if the subtype or pintype has a callback use it
	local callback = GetCallback(pinData.pinType, pinData.subType, "mouseclick")
	if callback then
		-- fire off the call back dont care about errors
		local success, _ = pcall(callback, pinData)
	end
	
end

--------------------------------------------------------------------------------
--# Event handler for Rbutton click on map icon, shows context menu from
--# MapMonster
--------------------------------------------------------------------------------
function MapMonster.OnMouseRightClickPin()

    local windowName = SystemData.ActiveWindow.name
	local pinId = WindowGetId(windowName)
	local pinData = GetPin(pinId)
	--local wpNavEnabled = false

	local function blank() end

	local function addWp() -- add as new WP if you have navigator installed
		MMNavigator.AddAsWaypoint(MapMonsterAPI.GetPin(pinId))
	end
	
	local function removeWp() -- add as new WP if you have navigator installed
		MMNavigator.RemoveAsWaypoint(MapMonsterAPI.GetPin(pinId))
	end
	
	local function hide() -- temporary hide
		if lastMapPinHighlight == windowName then
			WindowSetShowing(HighlightWindowName, false)
		end
		WindowSetShowing(windowName, false)
	end

	local function share() end -- no sharing subsystem yet
	local function edit() MapMonster.Editor.OpenEditWindow(pinId) end
	local function view() MapMonster.Editor.OpenViewWindow(pinId) end
	local function copy() end -- no copying yet
	local function lock() MapMonsterAPI.LockPin(pinId) end
	local function unlock() MapMonsterAPI.UnlockPin(pinId) end
	local function delete() MapMonsterAPI.DeletePin(pinId) end
	
	local function confirmDelete()
		local confirmLabel = L"Delete Map Pin:\n\n" .. towstring(pinData.label)
--DialogManager.MakeTwoButtonDialog( dialogText, button1Text, buttonCallback1, button2Text, buttonCallback2, timer, autoRespondButton, warningIsChecked, neverWarnCallback, dialogType, dialogID )
		DialogManager.MakeTwoButtonDialog( confirmLabel, L"Yes", delete, L"No", blank, nil)
	end

	EA_Window_ContextMenu.CreateContextMenu( windowName )
	if MMNavigator and MMNavigator.isWPEnabled then
		if MMNavigator.isWaypoint(pinId) then
			EA_Window_ContextMenu.AddMenuItem( L"Remove as Waypoint", removeWp, false, true )
		else
			EA_Window_ContextMenu.AddMenuItem( L"Add as Waypoint", addWp, false, true )
		end
	else
		EA_Window_ContextMenu.AddMenuItem( L"Navigator Disabled", blank, true, true )
	end
	--EA_Window_ContextMenu.AddMenuItem( L"Add as Waypoint",   addWp, not wpNavEnabled, true )
	EA_Window_ContextMenu.AddMenuItem( L"Hide MapPin", hide, false, true )
	EA_Window_ContextMenu.AddMenuItem( L"View Details", view, false, true )
	EA_Window_ContextMenu.AddMenuItem( L"Share", share, true,  true )
	if pinData.isLocked then
		EA_Window_ContextMenu.AddMenuItem( L"Unlock", unlock, false,  true )
	else
		EA_Window_ContextMenu.AddMenuItem( L"Lock", lock, false,  true )
		EA_Window_ContextMenu.AddMenuItem( L"Edit Details", edit, false, true )
		EA_Window_ContextMenu.AddMenuItem( L"Copy", copy, true, true )
  	  	EA_Window_ContextMenu.AddMenuItem( L"Delete", confirmDelete, false, true )
	end
    EA_Window_ContextMenu.Finalize()

end

--------------------------------------------------------------------------------
--# Event handler for zonemap filter toggle button, Lbutton click to toggle states 
--#
--------------------------------------------------------------------------------
function MapMonster.OnMouseLClickFilterButton()
	if MM_Settings.CycleSetting == MapMonster.SHOW_ALL then
		FilterButtonState(MapMonster.SHOW_DEFAULT)
	elseif MM_Settings.CycleSetting == MapMonster.SHOW_DEFAULT then
		FilterButtonState(MapMonster.SHOW_CUSTOM)
	elseif MM_Settings.CycleSetting == MapMonster.SHOW_CUSTOM then
		FilterButtonState(MapMonster.SHOW_ALL)
	end
end

function MapMonster.ResetFilters()
	for pinType, filtered in pairs( MM_Settings.Filters ) do
		MM_Settings.Filters[pinType] = true
	end
	forceZoneRefresh = true
end

--------------------------------------------------------------------------------
--# Event handler to zonemap filter toggle button, Rbutton click to show context
--# menu
--------------------------------------------------------------------------------
function MapMonster.ToggleFiltersMainMenu()
	if WindowGetShowing("EA_Window_ContextMenu1") then
		HideFilterMenus()
	else
		ShowFilterMenu( 1, Filter_MAIN_MENU )
	end
end

--------------------------------------------------------------------------------
--# Event handler for zonemap filter toggle button, Mouser over menu item
--# shows the sub context menu
--------------------------------------------------------------------------------
function MapMonster.ToggleFiltersSubMenu()
	-- if the last menu we Open isnt the main menu Hide it
	if (lastFilterMenuOpened ~= 1) then
		HideLastFilterSubMenus()
	end
	ShowFiltersSubMenu()
end

--------------------------------------------------------------------------------
--# Event handler for zonemap filter button mouseover, shows brief instructions
--# 
--------------------------------------------------------------------------------
function MapMonster.OnMouseOverFilterButton()

	-- TODO: this tooltip doesnt look right
    local windowName	= SystemData.ActiveWindow.name

    Tooltips.CreateTextOnlyTooltip (windowName, nil)
    Tooltips.SetTooltipText (1, 1,  L"Left Click to Toggle Filters\nRight Click to Open Filters Menu")
    Tooltips.SetTooltipColorDef (1, 1, Tooltips.COLOR_EXTRA_TEXT_DEFAULT)	
    
    Tooltips.Finalize()
    
    local anchor = { Point="bottom", RelativeTo="CursorWindow", RelativePoint="topleft", XOffset=0, YOffset=0 }
    Tooltips.AnchorTooltip (anchor)
    Tooltips.SetTooltipAlpha (1)
	
end

--------------------------------------------------------------------------------
--# Event handler for context menu checkbox to enable/disable filter
--#
--------------------------------------------------------------------------------
function MapMonster.ToggleFilterChoice()
	
    local checkBoxName = SystemData.MouseOverWindow.name
	local filterChoice = WindowGetId( checkBoxName )

	--[===[@alpha@
	d("Toggling! " .. filterChoice)
	--@end-alpha@]===]

	local mainIndex, subIndex = GetFilterMenuIndexes(filterChoice)
	local filterKey = FilterMenus[mainIndex][subIndex].filterKey
	--d(FilterMenus[mainIndex][subIndex])

	MM_Settings.Filters[filterKey] = not MM_Settings.Filters[filterKey]

	DisplayPinsForZone(lastZoneDisplayed["EA_Window_WorldMapZoneViewMapDisplay"])

end

function MapMonster.OnMouseRightClickFilter()
    local filterName = SystemData.MouseOverWindow.name
	local splitType = StringSplit(filterName, "_")
	local pinType = splitType[2]
	local subType = splitType[3]
	local contextTitle
	
	local function viewPinType()
		MapMonster.PinTypeEditor.OpenTypeViewer( pinType, subType )
	end
	local function editPinType()
		MapMonster.PinTypeEditor.OpenTypeEditor( pinType, subType )
	end
	
	if subType then
		contextTitle = MapMonsterAPI.GetPinTypeOption( pinType, subType, "label" )
		EA_Window_ContextMenu.CreateContextMenu( windowName , 3, contextTitle )
		EA_Window_ContextMenu.AddMenuDivider( 3 )
		EA_Window_ContextMenu.AddMenuItem( L"View Sub Type", viewPinType, false, true, 3 )
		EA_Window_ContextMenu.AddMenuItem( L"Edit Sub Type", editPinType, false, true, 3 )
	else
		contextTitle = MapMonsterAPI.GetPinTypeOption( pinType, "label" )
		EA_Window_ContextMenu.CreateContextMenu( windowName , 3, contextTitle )
		EA_Window_ContextMenu.AddMenuDivider( 3 )
		EA_Window_ContextMenu.AddMenuItem( L"View Pin Type", viewPinType, false, true, 3 )
		EA_Window_ContextMenu.AddMenuItem( L"Edit Pin Type", editPinType, false, true, 3 )
	end
	EA_Window_ContextMenu.Finalize( 3 )
end
--------------------------------------------------------------------------------
--#
--#			MapMonster API Functions
--#
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
--#	Function: MapMonsterAPI.CreatePinType(newPinType, pinTypeOptions, makeDefault)
--#		Create a Pin Type as main category for subsequent subTypes. Pin type 
--#		options serve as defaults for its subtypes. Make default if true or nil
--#		will create a default subtype identical to the new pin type
--#
--#	Parameters:
--#		newPinType	   -(string) String for new type
--#		pinTypeOptions -(table) Table of options to set for type
--#		makeDefault	   -(boolean) Flag to create default subtype at the same time
--#	Returns:
--#		boolean -(boolean) Return true on success or false otherwise
--#	Notes:
--#		Full details about valid options can be found at the top of this file or
--#		in the docs
--------------------------------------------------------------------------------
function MapMonsterAPI.CreatePinType(newPinType, pinTypeOptions, makeDefault)

	--[===[@alpha@
	d("MapMonsterAPI.CreatePinType(newPinType, pinTypeOptions)")
	--@end-alpha@]===]
	
	-- check pintype name
	if (newPinType == nil or type( newPinType ) ~= "string") then
		MapMonster.ERROR_CODE = 307
		return false
	end
	-- check for only alphanumeric in string
	if string.match(newPinType, "[^%w]") then
		MapMonster.ERROR_CODE = 308
		return false
	end

	-- check if pintype already exists
	if ( MapMonsterAPI.PinTypeIsRegistered(newPinType) ) then
		MapMonster.ERROR_CODE = 501
		return false
	end

	-- Create pin type
	PinTypes[newPinType] = {}
	PinTypes[newPinType].id = newPinType
	PinTypes[newPinType].subTypes = {}

	-- check pin type options
	if ( pinTypeOptions == nil or 
		 type(pinTypeOptions) ~= "table" )
	then
		pinTypeOptions = {}
	end

	-- Set pintype options using defaults if they are missing
	for optionId, optionData in pairs( DefaultPinTypeOptions ) do
		if pinTypeOptions[optionId] == nil then
			PinTypes[newPinType][optionId] = optionData
		else
			PinTypes[newPinType][optionId] = pinTypeOptions[optionId]
		end
	end

	if makeDefault == nil then
		makeDefault = true
	end
	if makeDefault then
		MapMonsterAPI.CreatePinSubType(newPinType, newPinType, pinTypeOptions)
	end

	return true

end

--------------------------------------------------------------------------------
--#	Function: MapMonsterAPI.CreatePinSubType(pinType, newPinSubType, subTypeOptions)
--#		Create a Pin Type as main category for subsequent subTypes. Pin type 
--#		options serve as defaults for its subtypes. Make default if true or nil
--#		will create a default subtype identical to the new pin type
--#
--#	Parameters:
--#		pinType	       -(string) Name of existing pin type
--#		newPinSubType  -(string) String for new sub type
--#		subTypeOptions -(table) Table of options to set for sub type, many optional
--#	Returns:
--#		boolean -(boolean) Return true on success or false otherwise
--#	Notes:
--#		Full details about valid options can be found at the top of this file or
--#		in the docs
--------------------------------------------------------------------------------
function MapMonsterAPI.CreatePinSubType(pinType, newPinSubType, subTypeOptions)

	--[===[@alpha@
	d("MapMonsterAPI.CreatePinSubType(pinType, newPinSubType, subTypeOptions)")
	--@end-alpha@]===]
	
	-- check if pintype already exists
	if not MapMonsterAPI.PinTypeIsRegistered(pinType) then
		MapMonster.ERROR_CODE = 207
		return false
	end

	-- check pinSubType name
	if (newPinSubType == nil or (type( newPinSubType ) ~= "string" and type( newPinSubType ) ~= "number") ) then
		MapMonster.ERROR_CODE = 309
		return false
	end
	newPinSubType = tostring(newPinSubType)

	-- check for only alphanumeric in string
	if string.match(newPinSubType, "[^%w]") then
		MapMonster.ERROR_CODE = 310
		return false
	end
	
	-- check if pinSubType already exists
	if MapMonsterAPI.PinTypeIsRegistered(pinType, newPinSubType) then
		MapMonster.ERROR_CODE = 502
		return false
	end
	
	-- assemble subType
	local subTypeEntry = {}
	subTypeEntry.id = newPinSubType
	subTypeEntry.label = subTypeOptions.label

	-- check and save subTypeOptions
	if ( subTypeOptions ~= nil and type(subTypeOptions) == "table" ) then
		for option, value in pairs(subTypeOptions) do
			-- if the option is different then the default for the pinType save it
			if ( PinTypes[pinType][option] ~= nil and 
	     		PinTypes[pinType][option] ~= value ) 
			then
				subTypeEntry[option] = LibToolkit.CopyObject(value)
			end
		end
	end
		
	-- make sure default subTypes arent saved to subType entries
	subTypeEntry.defaultSubType = nil
	-- Create subtype
	PinTypes[pinType].subTypes[newPinSubType] = subTypeEntry

	-- check the pin type has a defaultSubType otherwise set this as default
	if PinTypes[pinType].defaultSubType == "" then
		PinTypes[pinType].defaultSubType = newPinSubType
	end
	-- Reset filter menus
	InitializeFilters()
		
	return true

end

--------------------------------------------------------------------------------
--#	Function: MapMonsterAPI.PinTypeIsRegistered(checkPinType, checkPinSubType)
--#		Checks if the pintype and subtype are already registered.
--#
--#	Parameters:
--#		checkPinType    -(string) Pin type to look for
--#		checkPinSubType -(string) Optional: Sub type to look for
--#	Returns:
--#		boolean -(boolean) Return true is exists, false otherwise
--------------------------------------------------------------------------------
function MapMonsterAPI.PinTypeIsRegistered(checkPinType, checkPinSubType)
	for pinType, pinInfo in pairs( PinTypes ) do
		if (pinType == checkPinType) then
			if checkPinSubType then
				for subType, subTypeInfo in pairs( PinTypes[checkPinType].subTypes ) do
					if (subType == checkPinSubType) then
						return true
					end
				end
			else
				return true
			end
		end
	end
	return false
end

--------------------------------------------------------------------------------
--#	Function: MapMonsterAPI.GetPinTypes()
--#		Get a table of pin types currently registered does not include subtypes.
--#		Subtypes are listed in the pin type options
--#
--#	Parameters:
--#		none
--#	Returns:
--#		list -(table) Returns table of registered types
--------------------------------------------------------------------------------
function MapMonsterAPI.GetPinTypes()
	local list = {}
	for k,_ in pairs(PinTypes) do
		table.insert(list, k)
	end
	return list
end

--------------------------------------------------------------------------------
--#	Function: MapMonsterAPI.DeletePinType(pinType, subType)
--#		Deletes a pin type or subtype from MapMonster
--#
--#	Parameters:
--#		pinType -(string) Name of pin type to delete
--#		subType -(string) Optional: Name of sub type to delete
--#	Returns:
--#		boolean -(boolean) Return true on success or false otherwise
--#	Notes:
--#
--#		*WARNING*
--#		This isnt complete and will leave orphaned pins
--#		and there is no error checking or validation, much more work
--#		needed this will break things if you don't clean up yourself 
--#		*WARNING*
--#
--------------------------------------------------------------------------------
function MapMonsterAPI.DeletePinType(pinType, subType)

	--[===[@alpha@
	d("Delete PinType: " .. pinType .. ":" .. (subType or ""))
	--@end-alpha@]===]

	if not MapMonsterAPI.PinTypeIsRegistered(pinType, subType) then
		MapMonster.ERROR_CODE = 208
		return false
	end
	
	if subType then
		PinTypes[pinType].subTypes[subType] = nil
	else
		PinTypes[pinType] = nil
	end
	
	return true

end

--------------------------------------------------------------------------------
--#	Function: MapMonsterAPI.GetPinTypeOptions(pinType, subType)
--#		Retrieves a full set of pin options including any defaults from the main
--#		pin type.
--#
--#	Parameters:
--#		pinType -(string) Name of pin type to get options from
--#		subType -(string) Optional: Name of sub type to get options from
--#	Returns:
--#		options -(table) Table of options or false on failure
--------------------------------------------------------------------------------
function MapMonsterAPI.GetPinTypeOptions(pinType, subType)

	local pinTypeOptions

	if not MapMonsterAPI.PinTypeIsRegistered(pinType, subType) then
		MapMonster.ERROR_CODE = 208
		return false
	else
		pinTypeOptions = LibToolkit.CopyObject(PinTypes[pinType])
		if subType then
			for k, v in pairs(PinTypes[pinType].subTypes[subType]) do
				pinTypeOptions[k] = v
			end
			pinTypeOptions.subTypes = nil -- drop the subtypes from the subtype options
			pinTypeOptions.defaultSubType = nil -- drop the defaultsubtype from the subtype options
		end
	end

	if subType then
		pinTypeOptions.parent = MapMonsterAPI.GetPinTypeOptions(pinType)
	end

	return pinTypeOptions

end

--------------------------------------------------------------------------------
--#	Function: MapMonsterAPI.GetPinTypeOption(pinType, subType, optionKey)
--#		Retrieves a single option for pin type and subtype.
--#
--#	Parameters:
--#		pinType   -(string) Name of pin to retrieve from
--#		subType   -(string) Optional: Name of sub type to retrieve from
--#		optionKey -(string) Option name to retrieve
--#	Returns:
--#		value - Returns value or false on failure
--#	Notes:
--#
--#		*WARNING*
--#		Avoid using this one for isLocked or isPrivate for the moment since false
--#		would be a valid value for those and not an error
--------------------------------------------------------------------------------
function MapMonsterAPI.GetPinTypeOption(pinType, subType, optionKey)

	if optionKey == nil then
		optionKey = subType
		subType = nil
	end
	local options = MapMonsterAPI.GetPinTypeOptions(pinType, subType)
	if options then
		if options[optionKey] then
			return options[optionKey]
		else
			MapMonster.ERROR_CODE = 311
			return false
		end
	else
		return false
	end

end

--------------------------------------------------------------------------------
--#	Function: MapMonsterAPI.SetPinTypeOption( pinType, subType, optionKey, optionValue )
--#		Set pin type or subtype option, either by single key value pair or a table
--#		with multiple key value pairs
--#
--#	Parameters:
--#		pinType     -(string) Name of pin type to modify
--#		subType     -(string) Optional: Name of sub type to modify
--#		optionKey   -(string) Name of option to modify
--#		optionValue -(string) Value to set
--#	Returns:
--#		boolean -(boolean) Returns true on success or false on failure
--#	Notes:
--#		You can use a table of options with values instead of individual calls
--#		e.g. MapMonsterAPI.SetPinTypeOption( pinType, subType, table )
--#		or MapMonsterAPI.SetPinTypeOption( pinType, table )
--------------------------------------------------------------------------------
function MapMonsterAPI.SetPinTypeOption( pinType, subType, optionKey, optionValue )

	local needFilterRefresh = false
	-- get main pin type options
	if not MapMonsterAPI.PinTypeIsRegistered(pinType, subType) then
		MapMonster.ERROR_CODE = 208
		return false
	end

	local typeDefaultOptions
	local pinTypeOptions = PinTypes[pinType]
	
	-- switch args if we got a table as second arg
	if type(subType) == "table" then
		optionKey = subType
		subType =nil
	end
	-- switch args if were missing the value and didnt get a table
	if optionValue == nil and type(optionKey) ~= "table" then
		optionValue = optionKey
		optionKey = subType
		subType = nil
	end
	
	-- if we asked for a subtype get those options instead
	if subType then
		subType = tostring(subType)
		typeDefaultOptions = pinTypeOptions
		pinTypeOptions = PinTypes[pinType].subTypes[subType]
	end

	local function SaveValue(key, value)
		-- if its a valid key and value pair for pin type options
		if ValidateOptionValue(key, value) then
			-- do we have defaults to check against
			if typeDefaultOptions then
				-- same as default get rid of it
				if ( typeDefaultOptions[key] == pinTypeOptions[key] ) then
					pinTypeOptions[key] = nil
				else
					pinTypeOptions[key] = value
				end
			else
				pinTypeOptions[key] = value
			end
			return true
		else
			return false
		end
	end
		
	-- if we have a table of options as key pairs iterate them
	if ( type(optionKey) == "table" ) then
		for key, value in pairs( optionKey ) do
			if subType and key == "defaultSubType" then
				MapMonster.ERROR_CODE = 311
				return false
			else
				local success = SaveValue(key, value)
				if success and ( key == "label" or key == "mapIcon" ) then
					needFilterRefresh = true
				elseif not success then
					return false
				end
			end
		end
	else
		if subType and optionKey == "defaultSubType" then
			MapMonster.ERROR_CODE = 311
			return false
		else
			local success = SaveValue(optionKey, optionValue)
			if success and ( optionKey == "label" or optionKey == "mapIcon" ) then
				needFilterRefresh = true
			elseif not success then
				return false
			end
		end
	end

	if needFilterRefresh then
		InitializeFilters()
	end
	
	forceZoneRefresh = true
	
	return true

end

--------------------------------------------------------------------------------
--#	Function: MapMonsterAPI.DeletePinTypeOption( pinType, subType, optionKey )
--#		Delete an option from sub type, cannot delete any options from pin types
--#		since they are used as defaults for their sub types
--#
--#	Parameters:
--#		pinType   -(string) Name of pin type to modify
--#		subType   -(string) Name of pin sub type to modify
--#		optionKey -(string) Option to delete
--#	Returns:
--#		boolean -(boolean) Return true on success, false otherwise
--#	Notes:
--#
--------------------------------------------------------------------------------
function MapMonsterAPI.DeletePinTypeOption( pinType, subType, optionKey )

	if not subType or not optionKey then
		MapMonster.ERROR_CODE = 401
		return false
	end
		
	if not MapMonsterAPI.PinTypeIsRegistered(pinType, subType) then
		MapMonster.ERROR_CODE = 208
		return false
	end

	local pinSubTypeOptions = PinTypes[pinType].subTypes[subType]

	if ( type(optionKey) == "table" ) then
		MapMonster.ERROR_CODE = 402
		return false
	else
		if ( DefaultPinTypeOptions[optionKey] == nil ) then
			MapMonster.ERROR_CODE = 303
			return false
		else
			pinSubTypeOptions[optionKey] = nil
			return true
		end
	end

end

--------------------------------------------------------------------------------
--#	Function: MapMonsterAPI.CreatePin(pinType, subType, label, pinPos, note, addonData, merge)
--#		Validates and creates a new map pin. Can also specify arguments as a single table
--#
--#	Parameters:
--#		pinType  -(string) Name of pin type to make
--#		subType  -(string) Name of sub type to make
--#		label    -(wstring) Label for map pin, first line of tooltip as well
--#		pinPos   -(table) Table of position where to create the pin
--#		note     -(string) Optional: Note to add to the new pin
--#		addonData -(table) Optional: Any addon specific data to include in the pin
--#		merge   -(boolean) Optional: Flag to enable merging this pin with any existing pins
--#		
--#	Returns:
--#		pinId -(number) Returns the newly created pinId on success, false otherwise
--#	Notes:
--#		pinType, subType, label and position are mandatory but you can use a table
--#		as a single argument to CreatePin() as long as the table contains those keys
--#		Valid pin properties can be found at the top of this file or in the docs
--------------------------------------------------------------------------------
function MapMonsterAPI.CreatePin(pinType, subType, label, pinPos, note, addonData, merge)

	--[===[@alpha@
	d("Create Pin")
	--@end-alpha@]===]
		
	local pinData
	-- if we got a table as first arg, split up the details
	if ( type(pinType) == "table" ) then
		pinData = pinType
		pinType = pinData.pinType
		subType = pinData.subType
		label = pinData.label
		if (pinData.pinPos ~= nil and type(pinData.pinPos) == "table" ) then
			pinPos = pinData.pinPos
		else
			pinPos = { zoneId = pinData.zoneId,
					 worldX = pinData.worldX,
					 worldY = pinData.worldY,
					 zoneX  = pinData.zoneX,
					 zoneY  = pinData.zoneY, }
		end
		note = pinData.note
		addonData = pinData.addonData
		merge = pinData.merge
	end

	-- check we have good position to work with
	local success = MapMonsterAPI.ValidateWorldPos(pinPos)
	if not success then
		return false
	end

	pinType = tostring(pinType)
	subType = tostring(subType)
	
	-- check pin type and subtype
	if not MapMonsterAPI.PinTypeIsRegistered(pinType, subType) then
		MapMonster.ERROR_CODE = 208
		return false
	end

	-- check we have a basic label
	if label == nil or label == "" or label == L"" then
		MapMonster.ERROR_CODE = 209
		return false
	end

	-- get the zone pos if we can but don't fail
	local zonePos = MapMonsterAPI.WorldToZone(pinPos)
	if zonePos then
		pinPos.zoneX = zonePos.zoneX		
		pinPos.zoneY = zonePos.zoneY
	else
		pinPos.zoneX = nil
		pinPos.zoneY = nil
	end

	-- convert note to wstring
	if ( note == nil ) then
		note = DefaultPin.note
	elseif ( type(note) == "table" ) then
		-- if its a table leave it alone
	else
		note = { towstring(note) }
	end

	-- now we make the pin
	local newPin = {}
	newPin.id        = 0
	newPin.pinType   = pinType
	newPin.subType   = subType
	newPin.label     = label
	newPin.note      = note
	newPin.isLocked  = PinTypes[pinType].isLocked
	newPin.isPrivate = PinTypes[pinType].isPrivate
	newPin.zoneId    = pinPos.zoneId
	newPin.worldX    = pinPos.worldX
	newPin.worldY    = pinPos.worldY
	newPin.zoneX     = pinPos.zoneX
	newPin.zoneY     = pinPos.zoneY
	newPin.modified  = GetDatestamp()
	newPin.Data		 = addonData

	-- default merge to true
	if merge == nil then
		merge = true
	end

	return SavePin(newPin, merge)
	
end

--------------------------------------------------------------------------------
--#	Function: MapMonsterAPI.CreatePinAtPlayer(pinType, subType, label, note, addonData, merge)
--#		Convenience function to create a new pin at the players current position.
--#		See CreatePin() for details
--#
--#	Parameters:
--#		pinType  -(string) Name of pin type to make
--#		subType  -(string) Name of sub type to make
--#		label    -(wstring) Label for map pin, first line of tooltip as well
--#		pinPos   -(table) Table of position where to create the pin
--#		note     -(string) Optional: Note to add to the new pin
--#		addonData -(table) Optional: Any addon specific data to include in the pin
--#		merge   -(boolean) Optional: Flag to enable merging this pin with any existing pins
--#		
--#	Returns:
--#		pinId -(number) Returns the newly created pinId on success, false otherwise
--#
--#	Notes:
--#		See CreatePin() for details
--------------------------------------------------------------------------------
function MapMonsterAPI.CreatePinAtPlayer(pinType, subType, label, note, addonData, merge)
	return MapMonsterAPI.CreatePin( pinType, subType, label, MapMonsterAPI.GetPlayerPosition(), note, addonData, merge)
end

--------------------------------------------------------------------------------
--#	Function: MapMonsterAPI.GetPin(pinId)
--#		Returns a full copy of the pin specified, or false on failure
--#
--#	Parameters:
--#		pinId -(number) Pin Id were looking for
--#	Returns:
--#		pinData -(table) Returns full pin Data or false on failure
--------------------------------------------------------------------------------
function MapMonsterAPI.GetPin(pinId)

	if not pinId then
		MapMonster.ERROR_CODE = 210
		return false
	end
	if not PinStorage[pinId] then
		MapMonster.ERROR_CODE = 105
		return false
	end
	
	return LibToolkit.CopyObject( PinStorage[pinId] )
end

--------------------------------------------------------------------------------
--#	Function: MapMonsterAPI.DeletePin(pinId)
--#		Delete a specified pin completely
--#
--#	Parameters:
--#		pinId -(number) Pin Id we want to delete
--#	Returns:
--#		boolean -(boolean) Returns true on success, false otherwise
--------------------------------------------------------------------------------
function MapMonsterAPI.DeletePin(pinId)

	--[===[@alpha@
	d("Delete Pin: " .. pinId)
	--@end-alpha@]===]

	pinId = tonumber(pinId)
	pinData = GetPin(pinId)

	if pinData ~= false and not pinData.isLocked then
		-- If we have MapMonster Navigator make sure it drops any waypoints for this pin
		if MMNavigator and MMNavigator.isWPEnabled then
			MMNavigator.OnWaypointPinDelete(pinData)
		end		
		-- if the subtype or pintype has a delete callback use it
		local callback = GetCallback(pinData.pinType, pinData.subType, "delete")
		if callback then
			-- fire off the callback with a copy of the data, dont care about errors
			local success, _ = pcall(callback, MapMonsterAPI.GetPin(pinId))
		end
		DestroyPinIcon(pinId)
		PinStorage[pinId] = nil
		forceZoneRefresh = true
		return true
	else
		if pinData and pinData.isLocked then
			MapMonster.ERROR_CODE = 403
		end
		return false
	end

end

--------------------------------------------------------------------------------
--#	Function: MapMonsterAPI.GetPinProperty(pinId, propertyKey)
--#		Returns the value of specific pin property
--#
--#	Parameters:
--#		pinId -(number) Pin Id were looking for
--#		propertyKey -(string) Name of property were looking for
--#	Returns:
--#		propertyValue - Value of property speficied
--#	Notes:
--#		Needs more work here too, no checks or validation on property name, and
--#		have to account for boolean values, no failures for the moment
--#
--------------------------------------------------------------------------------
function MapMonsterAPI.GetPinProperty(pinId, propertyKey)
	local pinData = GetPin(pinId)
	return LibToolkit.CopyObject(pinData[propertyKey])
end

--------------------------------------------------------------------------------
--#	Function: MapMonsterAPI.SetPinProperty(pinId, propertyKey, propertyValue, updateDatestamp)
--#		Set a property on a individual pin
--#
--#	Parameters:
--#		pinId         -(number) Id of pin to modify
--#		propertyKey   -(string) Name of property to modify
--#		propertyValue - New value of property
--#		updateDatestamp - (boolean) Update the datestamp for pin, default is true
--#
--#	Returns:
--#		boolean -(boolean) Return true on success, false otherwise
--#
--#	Notes:
--#		You can modify several properties at once by using a table of key/value
--#		pairs as the second argument
--#		Changed in 0.3.2
--------------------------------------------------------------------------------
function MapMonsterAPI.SetPinProperty(pinId, propertyKey, propertyValue, updateDatestamp)

	--[===[@alpha@
	d("Set Pin Property")
	--@end-alpha@]===]
	
	local pinData = false
	local updateWaypoint = false
	
	-- if we get a table, shift the args around
	if type(pinId) == "table" then
		updateDatestamp = propertyKey
		propertyKey = pinId
		pinId = propertyKey.id
	end
	
	if pinId == nil then
		MapMonster.ERROR_CODE = 210
		return false
	end

	pinData = GetPin(pinId)
	if not pinData then
		return false
	end

	-- check if its locked first
	if pinData.isLocked then
		MapMonster.ERROR_CODE = 404
		return false
	end
	
	local function SaveValue(key, value)
		if key == "id" then
			return true -- skip it
		elseif key == "note" and type(value) ~= "table" then
				value = { towstring(value) }
		end
		if ValidatePinValue(key, value) then
			pinData[key] = value
			if  key == "label" or key == "zoneId" or 
				key == "worldX" or key == "worldY" or 
				key == "zoneX" or key == "zoneY"
			then
				updateWaypoint = true
			end			
			return true
		else
			return false
		end
	end

	if ( type(propertyKey) == "table" ) then
		for key, value in pairs( propertyKey ) do
			if not SaveValue(key, value) then
				return false
			end
		end
	else
		if not SaveValue(propertyKey, propertyValue) then
			return false
		end
	end

	-- Update the modified timestamp on every edit by default
	if updateDatestamp == nil or updateDatestamp == true then
		SaveValue("modified", GetDatestamp())
	end
	
	-- if the subtype or pintype has a edit callback use it
	local callback = GetCallback(pinData.pinType, pinData.subType, "edit")
	if callback then
		-- fire off the call back dont care about errors
		local success, _ = pcall(callback, MapMonsterAPI.GetPin(pinId))
	end
	
	-- If we have MapMonster Navigator make sure it drops any waypoints for this pin
	if updateWaypoint and MMNavigator and MMNavigator.isWPEnabled then
		MMNavigator.OnWaypointPinEdit(MapMonsterAPI.GetPin(pinId))
	end
	if (pinData.zoneId == lastZoneDisplayed["EA_Window_WorldMapZoneViewMapDisplay"]) then
		UpdatePinIcon(pinData.id)
		forceZoneRefresh = true
	end

	return true

end

--------------------------------------------------------------------------------
--#	Function: MapMonsterAPI.LockPin(pinId)
--#		Lock the pin, preventing any other modifications of the pin
--#
--#	Parameters:
--#		pinId -(number) Pin to lock
--#
--#	Returns:
--#		boolean -(boolean) Returns true on lock, false otherwise
--------------------------------------------------------------------------------
function MapMonsterAPI.LockPin(pinId)
	if not GetPin(pinId) then
		return false
	end
	PinStorage[pinId].isLocked = true
	return true
end

--------------------------------------------------------------------------------
--#	Function: MapMonsterAPI.UnlockPin(pinId)
--#		Unlock the pin, allowing the pin to be modified
--#
--#	Parameters:
--#		pinId -(number) Pin to unlock
--#
--#	Returns:
--#		boolean -(boolean) Returns true on unlock, false otherwise
--------------------------------------------------------------------------------
function MapMonsterAPI.UnlockPin(pinId)
	if not GetPin(pinId) then
		return false
	end
	PinStorage[pinId].isLocked = false
	return true
end

--------------------------------------------------------------------------------
--#	Function: MapMonsterAPI.GetPinsForZone(zoneId)
--#		Retrive table of all pins in specified zone
--#
--#	Parameters:
--#		zoneId -(number) Zone to get pin for
--#
--#	Returns:
--#		table -(table) Returns table of matching pins, or false for no pins
--#
--#	Notes:
--#		These are copies of the pin data, not references
--------------------------------------------------------------------------------
function MapMonsterAPI.GetPinsForZone(zoneId)

	local matchingPins = {}

	-- TODO: fix this clumsy linear search for pins in zone
	for pinId, pinData in pairs(PinStorage) do
		if ( pinData.zoneId == zoneId ) then
			matchingPins[pinId] = LibToolkit.CopyObject(PinStorage[pinId])
		end
	end

	if LibToolkit.IsTableEmpty(matchingPins) then
		return false
	end
	return matchingPins
end

--------------------------------------------------------------------------------
--#	Function: MapMonsterAPI.GetPinsForZoneBySubType(zoneId, pinType, subType)
--#		Retrieves a table of pins with matching zoneId, pin type and 
--#		sub type. If unlockedOnly is set the locked pins will be excluded
--#
--#	Parameters:
--#		zoneId -(number) Zone to get pin for
--#		pinType -(string) Pin type to filter by
--#		subType -(string) Sub type to filter by
--#
--#	Returns:
--#		table -(table) Returns table of matching pins, or false for no pins
--------------------------------------------------------------------------------
function MapMonsterAPI.GetPinsForZoneBySubType(zoneId, pinType, subType, unlockedOnly)

	local matchingPins = {}

	-- TODO: fix this clumsy linear search for similar pins
	for pinId, pinData in pairs(PinStorage) do
		if ( pinData.zoneId    == zoneId  and 
		     pinData.pinType   == pinType and 
		     pinData.subType   == subType ) 
		then
			if ( not unlockedOnly or 
				 ( unlockedOnly and pinData.isLocked == false ) )
			then
				table.insert( matchingPins, LibToolkit.CopyObject(PinStorage[pinId]) )
			end
		end
	end

	if LibToolkit.IsTableEmpty(matchingPins) then
		return false
	end
	return matchingPins
end

--------------------------------------------------------------------------------
--#	Function: MapMonsterAPI.GetPinsByType(pinType, subType)
--#		Retrive table of all pins of specified pinType or subType
--#
--#	Parameters:
--#		pinType -(string) Pin type to retrive
--#		subType -(string) Optional: Sub type to retrive
--#
--#	Returns:
--#		table -(table) Returns table of matching pins, or false for no pins
--#
--#	Notes:
--#		New in 0.3.2
--#		These are copies of the pin data, not references
--------------------------------------------------------------------------------
function MapMonsterAPI.GetPinsByType(pinType, subType)

	local success = MapMonsterAPI.PinTypeIsRegistered(pinType, subType)
	if not success then
		return false
	end
	
	local matchingPins = {}

	-- TODO: fix this clumsy linear search for pins in zone
	for pinId, pinData in pairs(PinStorage) do
		if subType then
			if pinData.pinType == pinType and pinData.subType == subType then
				matchingPins[pinId] = LibToolkit.CopyObject(PinStorage[pinId])
			end
		else
			if pinData.pinType == pinType then
				matchingPins[pinId] = LibToolkit.CopyObject(PinStorage[pinId])
			end			
		end
	end

	if LibToolkit.IsTableEmpty(matchingPins) then
		return false
	end

	return matchingPins

end

--------------------------------------------------------------------------------
--#	Function: MapMonsterAPI.HighlightMapPin(pinId)
--#		Opens the map to show selected pin with flashing highlight 
--#		around the map icon
--#
--#	Parameters:
--#		pinId -(number) Map Monster Pin to display
--#
--#	Returns:
--#		none
--#	Notes:
--#		New in 0.3.2
--------------------------------------------------------------------------------
function MapMonsterAPI.HighlightMapPin(pinId)
	local mapDisplay = "EA_Window_WorldMapZoneViewMapDisplay"
	local windowName = mapDisplay .. "MapMonster_MapPin" .. pinId
	local pinData = GetPin(pinId)

	-- make sure filters are on
	MM_Settings.Filters[pinData.pinType] = true
	MM_Settings.Filters[pinData.pinType .. ":" .. pinData.subType] = true

	if not WindowGetShowing("EA_Window_WorldMap") then
		WindowUtils.ToggleShowing( "EA_Window_WorldMap" )
	end

	EA_Window_WorldMap.SetMap( GameDefs.MapLevel.ZONE_MAP, pinData.zoneId )
	
	-- dont have the pin icon lets try a refresh
	if not DoesWindowExist(windowName) then
		forceZoneRefresh = true
		DisplayPinsForZone(lastZoneDisplayed[mapDisplay])
	end
	-- check for the pin again
	if DoesWindowExist(windowName) then	
		-- pop the map icon to the top
		WindowSetShowing(windowName, true)
		pinOnTop = pinId
		FlashMapPin(pinId)
	end

end

MapMonster.MainMapDisplays = {["EA_Window_WorldMapZoneViewMapDisplay"] = true}
--------------------------------------------------------------------------------
--#	Function: MapMonsterAPI.RegisterMainMapDisplay()
--#		Register additional Maps of Type SystemData.MapTypes.MAINMAP
--#     aka SystemData.MapTypes.NORMAL
--#	Parameters:
--#		name -(string)
--#	Returns:
--#		-
--------------------------------------------------------------------------------
function MapMonsterAPI.RegisterMainMapDisplay(name)
	MapMonster.MainMapDisplays[name] = true
	DisplayedMapPins[name] = {}
end

--------------------------------------------------------------------------------
--#	Function: MapMonsterAPI.UnregisterMainMapDisplay()
--#		Unregister additional Maps of Type SystemData.MapTypes.MAINMAP
--#     aka SystemData.MapTypes.NORMAL
--#	Parameters:
--#		name -(string)
--#	Returns:
--#		-
--------------------------------------------------------------------------------
function MapMonsterAPI.UnregisterMainMapDisplay(name)
	MapMonster.MainMapDisplays[name] = nil
	DisplayedMapPins[name] = nil
end

--------------------------------------------------------------------------------
--#	Function: MapMonsterAPI.HighlightMapPin(pinId)
--#		Request Display Icons for a MapDiplay
--#
--#	Parameters:
--#		name -(string) Map of Type SystemData.MapTypes.MAINMAP
--#     			aka SystemData.MapTypes.NORMAL
--#
--#	Returns:
--#		none
--#	Notes:
--#		
--------------------------------------------------------------------------------
function MapMonsterAPI.RequestDisplayPinsForZone(mapDisplay, zoneId) DisplayPinsForZone(zoneId, mapDisplay) end

function MapMonsterAPI.MapDisplaySetPinsShowing(mapDisplay, aBool)		-- temp hide function for minimaps that zoom to zonemsp, for example
	if not DisplayedMapPins[mapDisplay] then return end
	for pinId, _ in pairs(DisplayedMapPins[mapDisplay]) do
		local windowName = mapDisplay.."MapMonster_MapPin" .. tonumber(string.match(pinId, "%d+"))
		if DoesWindowExist(windowName) then
			WindowSetShowing(windowName, aBool)
		end
		--DisplayedMapPins[mapDisplay][pinId] = nil
	end
end
--------------------------------------------------------------------------------
--#
--#		Debugging functions don't use them
--#
--------------------------------------------------------------------------------
function MapMonster.NukePins()
	if not dangerWillRobinson then
		return
	end
	PinStorage = {}
	LastPinId = 0
	forceZoneRefresh = true
	DisplayPinsForZone(lastZoneDisplayed["EA_Window_WorldMapZoneViewMapDisplay"])
	d("BOOM! All Pins Destroyed")
end

function MapMonster.NukePinTypes()
	if not dangerWillRobinson then
		return
	end
	PinTypes = {}
	InitializeFilters()
	d("BOOM! All Pins Types Destroyed")
end

function MapMonster.NukeAll()
	if not dangerWillRobinson then
		return
	end
	MapMonster.NukePinTypes()
	MapMonster.NukePins()
end

function MapMonster.RebuildFilters()
	if not dangerWillRobinson then
		return
	end
	InitializeFilters()
end

